/*#include "../src/fonctions.h"*/
#include "../src/structures.h"
#include "../src/import.h"

#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#ifdef CURSES
#include <CUnit/CUCurses.h>
#else
#include <CUnit/Console.h>
#endif

void recursiveTest(Stack S, int depth) {
    int x, y, h1, h2;

    if (depth < 1000) {
	x = rand();
	y = rand();
	CU_ASSERT(stackLen(S) == depth);
	CU_ASSERT(push(S, x, y) == 1);

	CU_ASSERT(peek(S, &h1, &h2) == 1);
	CU_ASSERT(h1 == x);
	CU_ASSERT(h2 == y);

	recursiveTest(S, depth+1);

	CU_ASSERT(stackLen(S) == depth + 1);
	CU_ASSERT(pop(S, &h1, &h2) == 1);
	CU_ASSERT(stackLen(S) == depth);
	CU_ASSERT(h1 == x);
	CU_ASSERT(h2 == y);
    }
}


void testPile() {
    Stack S = stackInit();
    recursiveTest(S, 0);
    stackErase(S);
}

/*void testPlacement() { Ce test marche, mais les fonctions qu'il utilise ont cessé d'être exportées
    int i, id, rot;

    Moves placements = movesInit();
    Grille* grille = grilleInit(9);

    Case** holder = malloc(sizeof(Case*)*6);
    Case** holder2 = malloc(sizeof(Case*)*6);
    Case** holder3 = malloc(sizeof(Case*)*6);

    getCases(grille, NORMALE, 0, 0, holder);
    getCases(grille, DROITE, 4, 4, holder2);
    getCases(grille, INVERSE, 8, 8, holder3);

    CU_ASSERT(movesAdd(placements, 3, NORMALE, holder) == 0);
    CU_ASSERT(movesLen(placements) == 1);

    CU_ASSERT(movesAdd(placements, 4, DROITE, holder2) == 1);
    CU_ASSERT(movesLen(placements) == 2);

    CU_ASSERT(movesAdd(placements, 5, INVERSE, holder3) == 2);
    CU_ASSERT(movesLen(placements) == 3);

    Case** test;
    CU_ASSERT(movesRemove(placements, &id, &rot, &test) == 1);
    CU_ASSERT(movesLen(placements) == 2);

    for (i = 0; i < 6; i++)
	CU_ASSERT(test[i] == holder3[i]);

    grilleErase(grille);
    movesErase(placements);
    free(test);
}*/

void testAllocationGrille() {
    int taille, i, j;
    Grille* G;
    for (taille = 1; taille < 1000; taille += 3) {

	G = grilleInit(100);

	CU_ASSERT_FATAL(G != NULL);
	CU_ASSERT(G->taille == 100);
	for (i = 0; i < 100; i++) {
	    for (j = 0; j < 100; j++) {
		CU_ASSERT_FATAL(G->carte[i][j] != NULL);
		CU_ASSERT(G->carte[i][j]->couches != NULL);
		CU_ASSERT(G->carte[i][j]->x == i);
		CU_ASSERT(G->carte[i][j]->y == j);
	    }
	}

	grilleErase(G);
    }
}

/*void testAccesCases() { Ce test marche, mais les fonctions qu'il utilise ont cessé d'être exportées
    int i, j;
    Case* holder;
    Grille* G = grilleInit(100);

    for (i = -10; i < 110; i++) {
	for (j = -10; j < 110; j++) {
	    if (i >= 0 && j >= 0 && i < G->taille && j < G->taille) {
		CU_ASSERT(returnCase(G, i, j, &holder) == 1);
	    } else {
		CU_ASSERT(returnCase(G, i, j, &holder) == 0);
	    }
	}
    }

    grilleErase(G);
}*/

void testGenerationTuiles() {
    int i, pt;
    Tuile* tuile;
    char randomTerrains[6], terrains[6] = {'P', 'F', 'L', 'V', 'R', 'U'};

    for (i = 0; i < 100; i++) {
	for (pt = 0; pt < 6; pt++) {
	    randomTerrains[pt] = terrains[rand()%6];
	}

	tuile = tuileInit(i, randomTerrains);

	CU_ASSERT_FATAL(tuile != NULL);
	for (pt = 0; pt < 6; pt++) {
	    CU_ASSERT(tuile->terrains[pt] == randomTerrains[pt]);
	}

	tuileErase(tuile);
    }
}

void testGenerationRandomTuiles() {
    int i, j, pt, isIn;
    Tuile* tuile;
    char terrains[6] = {'P', 'F', 'L', 'V', 'R', 'U'};

    for (i = 0; i < 100; i++) {
	tuile = tuileInitRandom(i);

	CU_ASSERT_FATAL(tuile != NULL);


	for (pt = 0; pt < 6; pt++) {
	    isIn = 0;
	    for (j = 0; j < 6; j++) {
		if (tuile->terrains[pt] == terrains[j])
		    isIn = 1;
	    }

	    CU_ASSERT(isIn);
	}

	tuileErase(tuile);
    }
}

/*void testSortieTerrain() {
    FILE* p1 = fopen("./data/Partie1", "r");
    FILE* tuiles = fopen("./data/Tuiles", "r");
    Partie* P = loadPartieFromFile(p1, tuiles);

    Case* holder[6];

    CU_ASSERT(getCases(P->grille, INVERSE, 0, 0, holder) == 0);
    CU_ASSERT(getCases(P->grille, NORMALE, 0, 0, holder) == 1);
    partieErase(P);
}*/

void testCaseOperations() {
    int i, typeTerrain, numero;
    char historique[100];
    char terrains[6] = {'P', 'F', 'L', 'V', 'R', 'U'};
    Case* C = caseInit(0,0);

    for (i = 0; i < 100; i++) {
	caseUpdate(C, i, (historique[i] = terrains[rand()%6]));
    }

    for (i = 99; i >= 0; i--) {
	CU_ASSERT(getType(C) == historique[i]);
	CU_ASSERT(getMove(C) == i);
	CU_ASSERT(caseRemove(C, &numero, &typeTerrain) == 1);
	CU_ASSERT(numero == i);
	CU_ASSERT(typeTerrain == historique[i]);
    }
    
    caseErase(C);
}

/*void testGenPartieFile(){ Ce test marche, mais les fonctions qu'il utilise ont cessé d'être exportées
    const char* filePath = "./data/genTest.lvl";
    FILE* pTest = fopen(filePath, "w");
    genPartieFile(pTest);
    int fileRemovalStatus;
    \internal Test de la bonne création du fichier de test
    pTest = fopen(filePath, "r");
    FILE* tuiles = fopen("./data/Tuiles", "r");
    Partie* P = loadPartieFromFile(pTest, tuiles);

    Case* holder[6];

    CU_ASSERT(getCases(P->grille, INVERSE, 0, 0, holder) == 0);
    CU_ASSERT(getCases(P->grille, NORMALE, 0, 0, holder) == 1);
    partieErase(P);
    \internal Test de suppression du fichier de test
    fileRemovalStatus = remove(filePath);
    CU_ASSERT(fileRemovalStatus == 0);
    fclose(pTest);
    fclose(tuiles);
}*/

int main() {
    srand(time(NULL));
    if (CU_initialize_registry() == CUE_NOMEMORY) {
	fprintf(stderr, "Memory allocation failed\n");
	exit(1);
    }
    
    CU_pSuite def = CU_add_suite("Lot A", NULL, NULL);

    CU_add_test(def, "Maniement de piles", testPile);
    CU_add_test(def, "Allocation de la grille", testAllocationGrille);
    CU_add_test(def, "Generation de tuiles", testGenerationTuiles);
    CU_add_test(def, "Generation de tuiles aleatoires", testGenerationRandomTuiles);
    CU_add_test(def, "Test des operations sur le contenu des cases", testCaseOperations);
    /*CU_add_test(def, "Maniement de placements", testPlacement);
    CU_add_test(def, "Acces aux cases de la grille", testAccesCases);
    CU_add_test(def, "Test de limitations du terrain", testSortieTerrain);
    CU_add_test(def, "Test de limitation du terrain avec fichier de partie generes", testGenPartieFile);*/

#ifdef CURSES
    CU_curses_run_tests();
#else
    CU_console_run_tests();
#endif

    CU_cleanup_registry();
    return 0;
}
