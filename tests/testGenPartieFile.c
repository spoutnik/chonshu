#include "../src/fonctions.h"
#include <stdio.h>

int main(int argc,char** argv) {
	int STATUS;
	if (argc < 2){
		printf("WARNING | Veuillez spécifier un chemin de fichier !\n");
		STATUS = 1;
	}
	else{
		/*! \internal Generation du fichier passé en paramètre */
		if(genPartieFile(argv[1]) == 0){
			printf("OK | Fichier crée : %s\n",argv[1]);
			STATUS = 0;
		}
		else{
			printf("ERROR | Une erreur est survenue !\n");
			STATUS = 1;
		}
	}
	return STATUS;	
}
