
#include "../src/solveur.h"
#include "../src/import.h"

int main() {
    FILE* raw = fopen("data/Partie1", "r");
    FILE* tuil = fopen("data/Tuiles", "r");
    Partie* P = loadPartieFromFile(raw, tuil);
    fclose(raw);
    fclose(tuil);

    printf("max: %d\n", solve(P));
    dumpTerrain(stdout, P);
    dumpMoves(stdout, P);

    partieErase(P);
    return 0;
}

