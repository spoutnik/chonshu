#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 1024;

int main() {
    SDL_Window* window = NULL; 

    SDL_Surface* screenSurface = NULL;
    SDL_Surface* sprite = NULL;

    int i, j;
    SDL_Rect offset;

    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) { 
	printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() ); 
    } else {
	window = SDL_CreateWindow("SDL test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (window == NULL) {
	    printf("Window could not be created: SDL_Error: %s\n", SDL_GetError() );
	} else {
	    screenSurface = SDL_GetWindowSurface(window);

	    SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));

	    sprite = SDL_LoadBMP("../sprites/background.bmp");
	    for (i = 0; i < 32; i++) {
		offset.x = i*32;
		for (j = 0; j < 32; j++) {
		    offset.y = j*32;
		    SDL_BlitSurface(sprite, NULL, screenSurface, &offset);
		}
	    }

	    /*sprite = SDL_LoadBMP("../sprites/backgroundL.bmp");
	    SDL_BlitSurface(sprite, NULL, screenSurface, NULL);*/
	    
	    SDL_UpdateWindowSurface(window);
	    SDL_Delay(4000);
	}
    }

    SDL_FreeSurface(sprite);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
