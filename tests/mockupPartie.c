#include "../src/partie.h"
#include "../src/fonctions.h"

int main() {
    FILE* raw = fopen("data/Partie1", "r");
    FILE* tuiles = fopen("data/Tuiles", "r");
    Partie* P = loadPartieFromFile(raw, tuiles);
    fclose(raw);
    fclose(tuiles);

    tuileInsertion(P, 2, 1, 4, 5);
    tuileInsertion(P, 3, 3, 1, 7);
    tuileInsertion(P, 8, 1, 2, 7);
    tuileInsertion(P, 13, 3, 3, 8);
    tuileInsertion(P, 19, 1, 7, 5);
    tuileInsertion(P, 2, 2, 8, 5);
    tuileInsertion(P, 7, 2, 7, 4);
    tuileInsertion(P, 17, 2, 8, 4);
    tuileInsertion(P, 12, 0, 1, 3);
    tuileInsertion(P, 18, 0, 2, 1);
    tuileInsertion(P, 20, 1, 3, 0);
    tuileInsertion(P, 5, 1, 5, 2);

    nbPoints(P);
    nbPoints(P);

    partieErase(P);
    return 0;
}
