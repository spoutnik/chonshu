# Projet de C - Jeu de Honshu - LES BG

[Cahier des charges](http://www.ensiie.fr/~dimitri.watel/teaching/s2_project/subjects/01_honshu.pdf) - [Trello](https://trello.com/b/pUiLKJwx)

Coder un jeu de Honshu en C.

## Deadlines

- __Livrable 1, 25 mars:__ Lot A : Préparation des données  
    Responsable: Mehdi
- __Livrable 2, 8 avril:__ Lot B : Réalisation du jeu dans un terminal  
    Responsable: Yassir
- __Livrable 3, 29 avril:__ Lot C : Comptage et solveur  
    Responsable: JB
- __Livrable 4, 25 mai:__- Lot D : Jeu complet: interface graphique ou règles complexifiées  
    Responsable: Paul

## Concepts généraux

L'ensemble d'une partie de Honshu est contenu dans l'objet `Partie`. Il s'y trouve:

- Une `Grille` de `Cases`: Le plateau de jeu;
- Un `deck`: La liste des tuiles disponibles, de taille `nbTuiles`;
- Une liste d'entiers, qui indique de combien de chaque type de tuile on dispose;
- Une pile `Moves`: la liste des actions effectuées par le joueur.

Comment est relié tout ça ?

Chaque action est enregistré dans la pile de `Moves` de la partie. Elle prend cette forme:

| Numéro | Tuile | Rotation | Emplacement |
|--------|-------|----------|-------------|
| 1	     | 3     | NORMALE  | [Case* c1, Case* c2 .. Case* c6] |
| ...    | ...   | ...      | ...         |

Ainsi on sait où chaque tuile à été placée, dans quel sens, à quel moment.

Dans chaque `Case` du tableau se trouve un autre type de pile, du couples d'entiers cette fois; de type `(idMove, terrain)`. Elle est mise à jour à chaque fois que la case est couverte par une nouvelle `Tuile`. En regardant les éléments de cette pile, on a l'historique des placements par lesquelles la case à été affectée et les terrains qu'elle a reçu. En allant chercher le numéro de ce placement dans la pile de la `Partie`, on obtient le groupe de `Case`s affectée par une tuile: pratique pour faire des tests de recouvrement. 

## Orientations des tuiles et numérotation des cases

```
 
 NORMALE | GAUCHE | INVERSE | DROITE
---------|--------|---------|--------
   1 2   |  2 4 6 |   6 5   |  5 3 1
   3 4   |  1 3 5 |   4 3   |  6 4 2
   5 6   |        |   2 1   |
 
```

## Git

```sh
## Modifier le code

# Pour télécharger la dernière version:
git pull

# Pour modifier du code, créer une nouvelle branche:
git checkout -b <nom de la branche>

# Pour donner à git un fichier:
git add <fichier>

# Pour donner à git tous les fichiers:
git add .

# Pour créer une nouvelle version:
git commit

# Une fois que tout marche,
git push

## Autres commandes

git log		# Voir les dernières actions effectuées
git diff <file> # Voir ce qui a changé depuis le dernier commit
git status	# Lister les fichiers modifiés
git branch	# Lister les branches
```

## Doxygen

Pour consulter la documentation, ouvrir avec un browser `<chonshu>/doc/html/index.html`.

```sh
make dox
```

## Installation

```sh
make install
```

## Lancement du jeu 

Lancement : 
```sh
./chonshu
```

Choix du fichier de partie (Partie1, Partie2 ou Partie3):
```sh
data/Partie* 
 ```

Choix du fichier de tuiles :
```sh
data/Tuiles
```

Lancement de la partie avec un fichier de partie aléatoire :
```sh
./chonshu rand
```