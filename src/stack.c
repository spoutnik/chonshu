#include "stack.h"
#include <stdio.h>

Stack stackInit() {
    Stack S = (Stack) malloc(sizeof(struct link*));
    *S = NULL;
    return S;
}

Stack stackCopy(Stack S) {
    int x, y;
    Stack buffer = stackInit(), newStack = stackInit();

    while (pop(S, &x, &y))
	push(buffer, x, y);

    while (pop(buffer, &x, &y)) {
	push(S, x, y);
	push(newStack, x, y);
    }

    stackErase(buffer);
    return newStack;
}

void stackErase(Stack S) {
    int x, y;
    while (pop(S, &x, &y));
    free(S);
}

int push(Stack S, int x, int y) {
    struct link* new_el = (struct link*) malloc(sizeof(struct link));
    new_el->x = x;
    new_el->y = y;
    new_el->next = *S;
    *S = new_el;
    return 1;
}
    
int pop(Stack S, int* x, int* y) {
    if (*S == NULL)
	return 0;
    else {
	struct link* tmp = *S;
	*S = (*S)->next;

	*x = tmp->x;
	*y = tmp->y;

	free(tmp);
	return 1;
    }
}

int peek(Stack S, int* x, int* y) {
    if (pop(S, x, y)) {
	push(S, *x, *y);
	return 1;
    } else {
	return 0;
    }
}

int isIn(Stack S, int x, int y) {
    struct link* current = *S;

    while (current) {
	if (current->x == x && current->y == y)
	    return 1;
	current = current->next;
    }

    return 0;
}

int stackLen(Stack S) {
    struct link* current = *S;
    int size = 0;

    while (current) {
	size++;
	current = current->next;
    }

    return size;
}

void stackMerge(Stack dest, Stack toMerge) {
    struct link* current = *dest;

    if (current) {
	while (current->next)
	    current = current->next;
    }

    current->next = (*toMerge)->next;

    free(toMerge);
}
