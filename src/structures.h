#ifndef CHONSHU_STRUCTURE_H
#define CHONSHU_STRUCTURE_H

#include "stack.h"

/*! \brief Elément d'une liste de Stack
 * \warning Ne pas utiliser tel quel ! */

struct linkStructures {

    /*! \brief le contenu de l'élément */

    Stack casesList;

    /*! \brief Un pointeur vers l'élément suivant
     *
     * `NULL` indique la fin de la pile. */

    struct linkStructures* next;
};

/*! \brief Structure de liste de piles */

typedef struct linkStructures** Structures;

/*! \brief Initialise une liste de piles */

Structures structuresInit();

/*! \brief Détruit une liste de piles
 * \param S La liste de piles à détruire */

void structuresErase(Structures S);

/*! \brief Ajoute un élément à la liste de piles
 *
 * \param S La liste de piles à modifier
 * \param casesList La pile à ajouter
 *
 * \return `1` si l'élément à pu être inséré, `0` sinon. */

int structuresAdd(Structures S, Stack casesList);

/*! \brief Enlève un élément d'une liste de piles
 *
 * \param S La liste de piles à modifier
 * \param holder Un pointeur où stocker le conteu de la pile à enlever
 * \param index L'index de l'élément à enlever
 *
 * \return `1` si l'élément à pu être enlevé, `0` sinon. */

int structuresDel(Structures S, Stack* holder, int index);

/*! \brief Test de présence dans la liste de piles d'un couple (`x`,`y`)
 *
 * \param S La liste de piles à tester
 * \param x Le premier membre du couple
 * \param y Le second membre du couple
 *
 * \return `1` si l'élément est présent dans la liste, `0` sinon. */

int structuresCheck(Structures S, int x, int y);

/*! \brief Taille de la liste de piles
 *
 * \param S La liste de piles
 *
 * \return Sa taille */

int structuresLen(Structures S);

/*! \brief Vide une liste de piles
 *
 * \param S La liste de piles à vider */

void structuresEmpty(Structures S);

/*! \brief La taille maximale des piles dans la liste
 *
 * \param listStructures La liste de piles à tester
 * \param index Un pointeur où stocker l'index de la plus grande structure
 *
 * \return La taille maximale des piles dans la liste */

int tailleMax(Structures listStructures, int* index);

#endif
