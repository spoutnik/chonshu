#include "../solveur.h"
#include "../import.h"

int main() {
    FILE* raw = readFile("Quelle partie dois-je importer ?");
    FILE* tuiles = readFile("Quelles tuiles dois-je importer ?");
    Partie* P = loadPartieFromFile(raw, tuiles);
    fclose(raw);
    fclose(tuiles);
    int i, tuilesAPlacer = nbTuilesRestantes(P);

    for (i = 0; i < tuilesAPlacer; i++) {
	partialSolve(P, 1);
	while(enleverTuile(P));
    
	FILE* instructions = fopen("optimalMoves.lvl", "r");
	rebuild(instructions, P);
	fclose(instructions);
    }

    nbPoints(P, 1);
    dumpTerrain(stdout, P);

    partieErase(P);
    return 0;
}

