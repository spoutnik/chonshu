#include "../ui.h"
#include "../import.h"

int main(int argc, char** argv) {
    int c = 0;
    srand(time(NULL));
 
    Partie* P = NULL;

    if (argc > 1 && !strcmp(argv[1], "-h")) {
	printf ("Usage: ./chonshu	pour une partie classique\n");
	printf ("Usage: ./chonshu rand  pour une partie aléatoire\n");
	exit(0);
    }

    if (argc > 1 && !strcmp(argv[1], "rand")) {
	FILE* tuiles = readFile("Quelles tuiles dois-je importer ?");
	P = loadPartieFromRand(tuiles);
    } else {
#ifndef DEBUG
	FILE* raw = readFile("Quelle partie dois-je importer ?");
	FILE* tuiles = readFile("Quelles tuiles dois-je importer ?");
#else
	FILE* raw = fopen("./data/Partie1", "r");
	FILE* tuiles = fopen("./data/Tuiles", "r");
#endif
	P = loadPartieFromFile(raw, tuiles);
    }


    init();
    refresh();

    Display D = initDisplay(P);
    putMap(D);
    putCursor(D);

    displayRest(D);
    dRefresh(D);
    doupdate();

    while (c != 'q' && !partieFin(P)) {
	c = getch();

	interpret(D, c);

	putMap(D);
	putCursor(D);
	displayRest(D);
	logMoves(D);
        /*char msg[15];
        sprintf(msg,"Score : %3d",nbPoints(P,0));
        logWin(D->scorePane,msg);*/
	dRefresh(D);
	doupdate();
    }

    done();
    dumpTerrain(stdout, P);
    nbPoints(P, 1);
    partieErase(P);
    return 0;
}
