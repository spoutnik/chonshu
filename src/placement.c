#include "placement.h"

Moves movesInit() {
    Moves M = (Moves) malloc(sizeof(struct el*));
    *M = NULL;
    return M;
}

void movesErase(Moves S) {
    int g1, g2;
    Case** g3;

    while (movesRemove(S, &g1, &g2, &g3)) {
	free(g3);
    }

    free(S);
}

void movesSoftErase(Moves S) {
    int g1, g2;
    Case** g3;

    while (movesRemove(S, &g1, &g2, &g3));

    free(S);
}

int movesAdd(Moves S, int idTuile, int rotation, Case** emplacement) {
    struct el* newMove = (struct el*) malloc(sizeof(struct el));

    newMove->moveNumber = (*S == NULL ? 0 : (*S)->moveNumber + 1);
    newMove->idTuile = idTuile;
    newMove->rotation = rotation;
    newMove->emplacement = emplacement;
    newMove->next = *S;

    *S = newMove;
    return (*S)->moveNumber;
}

int movesRemove(Moves S, int* idTuile, int* rotation, Case*** emplacement) {
    struct el* tmp = *S;

    if (tmp == NULL)
	return 0;

    *S = (*S)->next;

    *idTuile = tmp->idTuile;
    *rotation = tmp->rotation;
    *emplacement = tmp->emplacement;

    free(tmp);

    return 1;
}

int movesData(Moves S, int index, int* idTuile, int* rotation, Case*** emplacement) {
    struct el* current = *S;

    if (current == NULL || current->moveNumber < index)
	return 0;

    while (current->moveNumber > index)
	current = current->next;

    *idTuile = current->idTuile;
    *rotation = current->rotation;
    *emplacement = current->emplacement;

    return 1;
}

int movesIdTuile(Moves M, int index) {
    int idTuile, rotation;
    Case** emplacement;

    movesData(M, index, &idTuile, &rotation, &emplacement);
    return idTuile;
}

int movesRotation(Moves M, int index) {
    int idTuile, rotation;
    Case** emplacement;

    movesData(M, index, &idTuile, &rotation, &emplacement);
    return rotation;
}

Case** movesEmplacement(Moves M, int index) {
    int idTuile, rotation;
    Case** emplacement;

    movesData(M, index, &idTuile, &rotation, &emplacement);
    return emplacement;
}

int movesLen(Moves S) {
    return (*S == NULL ? 0 : (*S)->moveNumber + 1);
}
