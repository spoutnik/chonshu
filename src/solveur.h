#ifndef CHONSHU_SOLVEUR_H
#define CHONSHU_SOLVEUR_H

#include "partie.h"
#include "import.h"

/*! \brief Résoudre une partie
 *
 * \param P La partie à résoudre
 *
 * \return Le max de points atteignables 
 *
 * Complexité incroyable, prend plus de temps à sortir un résultat qu'un univers à exister */

int solve(Partie* P);

/*! \brief Résoudre une partie sur maxDepth étapes
 *
 * \param P La partie à résoudre
 * \param maxDepth Le nombre d'étapes à considérer
 *
 * \return Le meilleur score atteignable en `maxDepth` étapes
 *
 * `partialSolve(P, nbTuiles(P))` est l'implémentation de `solve(P)` */

int partialSolve(Partie* P, int maxDepth);

#endif
