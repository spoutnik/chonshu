#include "ui.h"
#include <locale.h>

void init() {
    initscr();		    /* Initialise le screen */
    raw();
    keypad(stdscr, TRUE);
    noecho();		    /* qd tu touche une touche ne saffiche pas  */
    start_color();	    /* ca start les couleurs */

    curs_set(0);	    /* efface curseur du terminal */

    init_pair(1, COLOR_GREEN, COLOR_BLACK); /* initialisation des couleurs par paire couleur ecriture et couleur fond */
    init_pair(2, COLOR_RED, COLOR_BLACK);
}

void dRefresh(Display D) {
    wnoutrefresh(D->board);
    wnoutrefresh(D->select);
    logMoves(D);
    wnoutrefresh(D->status);
    wnoutrefresh(D->menuBar);
    wnoutrefresh(D->scorePane);
}

WINDOW* createWindow(int height, int width, int y, int x) {
    WINDOW* newWin;

    newWin = newwin(height, width, y, x);
    box(newWin, 0, 0);	    /* trace les lignes autour et caractère normaux*/

    return newWin;
}

Display initDisplay(Partie* P) {
    int sep1 = 21;
    Display newDisplay = malloc(sizeof(struct ui));

    newDisplay->board = createWindow(LINES-4 , COLS - sep1, 0, sep1);
    newDisplay->select = createWindow((LINES/2)-2, sep1, 0, 0);
    newDisplay->status = createWindow((LINES/2)-2, sep1, (LINES/2)-2, 0);
    newDisplay->rules = createWindow(LINES - 10, COLS - 10, 5, 5);
    newDisplay->scorePane = createWindow(4, sep1, LINES-4,0);
    newDisplay->menuBar = createWindow(4, COLS - sep1, LINES-4,sep1);

    newDisplay->panels[0] = new_panel(newDisplay->board);
    newDisplay->panels[1] = new_panel(newDisplay->select);
    newDisplay->panels[2] = new_panel(newDisplay->status);
    newDisplay->panels[3] = new_panel(newDisplay->rules);
    newDisplay->panels[4] = new_panel(newDisplay->scorePane);
    newDisplay->panels[5] = new_panel(newDisplay->menuBar);

    bottom_panel(newDisplay->panels[3]);
    logWin(newDisplay->menuBar, "Appuyez sur 'h' pour l'aide");
    /*update_panels();
*/
    newDisplay->partie = P;

    newDisplay->cursory = 0;
    newDisplay->cursorx = 1;
    newDisplay->tRot = NORMALE;
    newDisplay->idTuile = 0;

    setTitle(newDisplay->status, "[Placements]");
    setTitle(newDisplay->select, "[Tuiles]");
    setTitle(newDisplay->board, "[Grille]");
    setTitle(newDisplay->rules, "[Regles du jeu]");
    setTitle(newDisplay->scorePane, "[Score]");
    update_panels();

    initCursor(newDisplay);
    return newDisplay;
}

void logWin(WINDOW* win, char* msg) {
    int x, y;
    getmaxyx(win, y, x);
    x++;

    mvwprintw(win, y - 2, 2, msg);
}

int done() {
    endwin();
    return 0;
}

void putTuile(WINDOW* win, Partie* P, int idTuile, int x, int y, int rotation) {
    switch (rotation) {
	case NORMALE:
	    mvwaddch(win, y, x, P->deck[idTuile]->terrains[0]); /* ajoute le caractère en posit (x;y) */
	    mvwaddch(win, y, x+2, P->deck[idTuile]->terrains[1]);
	    mvwaddch(win, y+1, x, P->deck[idTuile]->terrains[2]);
	    mvwaddch(win, y+1, x+2, P->deck[idTuile]->terrains[3]);
	    mvwaddch(win, y+2, x, P->deck[idTuile]->terrains[4]);
	    mvwaddch(win, y+2, x+2, P->deck[idTuile]->terrains[5]);
	    break;

	case GAUCHE:
	    mvwaddch(win, y, x, P->deck[idTuile]->terrains[0]);
	    mvwaddch(win, y-1, x, P->deck[idTuile]->terrains[1]);
	    mvwaddch(win, y, x+2, P->deck[idTuile]->terrains[2]);
	    mvwaddch(win, y-1, x+2, P->deck[idTuile]->terrains[3]);
	    mvwaddch(win, y, x+4, P->deck[idTuile]->terrains[4]);
	    mvwaddch(win, y-1, x+4, P->deck[idTuile]->terrains[5]);
	    break;

	case DROITE:
	    mvwaddch(win, y, x, P->deck[idTuile]->terrains[0]);
	    mvwaddch(win, y+1, x, P->deck[idTuile]->terrains[1]);
	    mvwaddch(win, y, x-2, P->deck[idTuile]->terrains[2]);
	    mvwaddch(win, y+1, x-2, P->deck[idTuile]->terrains[3]);
	    mvwaddch(win, y, x-4, P->deck[idTuile]->terrains[4]);
	    mvwaddch(win, y+1, x-4, P->deck[idTuile]->terrains[5]);
	    break;

	case INVERSE:
	    mvwaddch(win, y, x, P->deck[idTuile]->terrains[0]);
	    mvwaddch(win, y, x-2, P->deck[idTuile]->terrains[1]);
	    mvwaddch(win, y-1, x, P->deck[idTuile]->terrains[2]);
	    mvwaddch(win, y-1, x-2, P->deck[idTuile]->terrains[3]);
	    mvwaddch(win, y-2, x, P->deck[idTuile]->terrains[4]);
	    mvwaddch(win, y-2, x-2, P->deck[idTuile]->terrains[5]);
	    break;
    }
}

void putCursor(Display D) {
    D->validMove = valide(D->partie, (D->cursorx - D->gridX)/2, D->cursory - D->gridY, D->tRot);
    wattron(D->board, A_BOLD);  /* agit sur une fenetre et change type ecriture ici en gras par ex */

    if (D->validMove == 1)
	wattron(D->board, COLOR_PAIR(1));
    else
	wattron(D->board, COLOR_PAIR(2));

    putTuile(D->board, D->partie, D->idTuile, D->cursorx, D->cursory, D->tRot);

    wattroff(D->board, COLOR_PAIR(1));
    wattroff(D->board, COLOR_PAIR(2));
    wattroff(D->board, A_BOLD);
} 

void initCursor(Display D) {
    int x, y;
    int taille = D->partie->grille->taille;

    getmaxyx(D->board, y, x);

    D->cursorx = (x - 2*taille)/2 + 2*(taille/2);
    D->cursory = (y - taille)/2 + taille/2 - 1;

    D->gridX = (x - 2*taille)/2;
    D->gridY = (y - taille)/2;
    D->gridSize = taille;
    D->idTuile = getNextTuile(D->partie, D->partie->nbTuiles-1);
}

void updateCursor(Display D, int dx, int dy) {
    switch (D->tRot) {
	case NORMALE:
	    if (!(D->cursorx + dx > D->gridX - 1 
		    && D->cursorx + dx < D->gridX + 2*D->gridSize - 2
		    && D->cursory + dy > D->gridY - 1 
		    && D->cursory + dy < D->gridY + D->gridSize -2
		 )) return;
	    break;

	case DROITE:
	    if (!(D->cursorx + dx > D->gridX + 3 
		    && D->cursorx + dx < D->gridX + 2*D->gridSize
		    && D->cursory + dy > D->gridY - 1 
		    && D->cursory + dy < D->gridY + D->gridSize -1
		 )) return;
	    break;

	case GAUCHE:
	    if (!(D->cursorx + dx > D->gridX -1 
		    && D->cursorx + dx < D->gridX + 2*D->gridSize -4
		    && D->cursory + dy > D->gridY 
		    && D->cursory + dy < D->gridY + D->gridSize 
		 )) return;
	    break;

	case INVERSE:
	    if (!(D->cursorx + dx > D->gridX + 1
		    && D->cursorx + dx < D->gridX + 2*D->gridSize
		    && D->cursory + dy > D->gridY + 1
		    && D->cursory + dy < D->gridY + D->gridSize 
		 )) return;
	    break;
    }

    D->cursorx += dx;
    D->cursory += dy;
}

void updateRot(Display D) {
    switch (D->tRot) {
	case NORMALE:
	    if (D->cursorx < D->gridX + 4) 
		return;
	    logWin(D->board, "Rotation gauche\t\t");
	    break;
    
	case DROITE:
	    if (D->cursory < D->gridY + 2) 
		return;
	    logWin(D->board, "Rotation inverse\t\t");
	    break;
    
	case GAUCHE:
	    if (D->cursory > D->gridY + D->gridSize - 3) 
		return;
	    logWin(D->board, "Rotation normale\t\t");
	    break;

	case INVERSE:
	    if (D->cursorx > D->gridX + 2*D->gridSize - 5) 
		return;
	    logWin(D->board, "Rotation droite\t\t");
	    break;
    }
    
    D->tRot = (D->tRot + 1)%4;
}

void interpret(Display D, int c) {
    FILE* instructions;
    char msg[15];
    logWin(D->board , "                                         ");

    switch(c) {
	case KEY_DOWN:
	    updateCursor(D, 0, 1);
	    break;

	case KEY_UP:
	    updateCursor(D, 0, -1);
            break;

	case KEY_LEFT:
	    updateCursor(D, -2, 0);
	    break;

      	case KEY_RIGHT:
	    updateCursor(D, 2, 0);
  	    break;

	case 'b':
	    instructions = fopen("optimalMoves.lvl", "r");
	    rebuild(instructions, D->partie);
	    fclose(instructions);
	    break;

	case 'h':
	    showRules(D);
	    break;

	case 'r':
	    updateRot(D);
	    break;

	case 'n':
	    D->idTuile = getNextTuile(D->partie, D->idTuile);
	    mvwprintw(D->board, 2, 2, "Tuile: %2d", D->idTuile);
	    break;

	case '?':
	    partialSolve(D->partie, 1);
	    instructions = fopen("optimalMoves.lvl", "r");
	    while(enleverTuile(D->partie));
	    rebuild(instructions, D->partie);
	    fclose(instructions);
	    sprintf(msg, "Score: %3d", nbPoints(D->partie, 0));
	    logWin(D->scorePane, msg);
	    break;

	case 'x':
	    enleverTuile(D->partie);
	    eraseWin(D->status);
	    sprintf(msg, "Score: %3d", nbPoints(D->partie, 0));
	    logWin(D->scorePane, msg);
	    break;
	
	case 'p':
	    if (D->validMove == 1) {
		tuileInsertion(D->partie, D->idTuile, D->tRot, (D->cursorx - D->gridX)/2, D->cursory - D->gridY);
		if (getNextTuile(D->partie, D->idTuile) != -1) {
		    D->idTuile = getNextTuile(D->partie, D->idTuile);
		    mvwprintw(D->board, 2, 2, "Tuile: %2d", D->idTuile);
		}
	    } else {
		logWin(D->board, "This move is not valid ! Dumbass :( ");
	    }
	    sprintf(msg, "Score: %3d", nbPoints(D->partie, 0));
	    logWin(D->scorePane, msg);
	    break;
	
	default:
	    logWin(D->board, "Appuyez sur 'h' pour afficher l'aide");
    }
}

void putMap(Display D) {
    Grille* G = D->partie->grille;
    char* buf = malloc(sizeof(char)*2*G->taille+1);
    memset(buf, '\0', sizeof(char)*2*G->taille+1);
    int x, y, i, j;

    getmaxyx(D->board, y, x);

    x = (x - 2*G->taille)/2;
    y = (y - G->taille)/2;
    
    wattron(D->board, A_BOLD);
    for (i = 0; i < G->taille; i++) {
	for (j = 0; j < G->taille; j++) {
	    buf[j*2] = getType(G->carte[j][i]);
	    buf[j*2+1] = ' ';
	}
	mvwprintw(D->board, y+i, x, "%s", buf);
    }
    wattroff(D->board, A_BOLD);

    free(buf);
}

void setTitle(WINDOW* win, char* title) {
    mvwprintw(win, 0, 1, title);
}

void eraseWin(WINDOW* win) {
	int x, y, i, j;
	getmaxyx(win, y, x);
	
	for (i = 1; i < x-1; i++) {
		for (j = 1; j < y-1; j++) {
			mvwprintw(win, j, i, " ");
		}
	}
}

void logMoves(Display D) {
    Moves placements = D->partie->placements;
    int x, y, i, len = movesLen(placements);
    int idTuile, rotation;
    Case** holder;

    eraseWin(D->status);
    getmaxyx(D->status, y, x);
    x++;
    y++;

    for (i = 0; i < len; i++) {
	movesData(placements, i, &idTuile, &rotation, &holder);
	mvwprintw(D->status, (i + 1), 2, "Tuile %d en %d/%d", idTuile, holder[0]->x, holder[0]->y);
    }
}

void displayRest(Display D) {
    int i, ord = 3, abs = 3;
    eraseWin(D->select);

    for (i = 0; i < D->partie->nbTuiles; i++) {
	if (D->partie->tuilesDisponibles[i] != 0) {
	    mvwprintw(D->select, ord, abs, "Tuile %d: %d", D->partie->deck[i]->id, D->partie->tuilesDisponibles[i]);
	    ord++;
	}
    }
}

void showRules(Display D) {
    int pos = 2;
    setlocale(LC_ALL, "");
    top_panel(D->panels[3]);

    mvwprintw(D->rules, pos++, 3, "Etendez votre domaine en plaçant les tuiles de terrain à votre disposition.");

    pos++;

    mvwprintw(D->rules, pos++, 3, "Légende :");
    mvwprintw(D->rules, pos++, 6, "V : Village; F : Forêt; L : Lac; P : Plaine; R : Ressource; U : Usine.");

    pos++;

    mvwprintw(D->rules, pos++, 3, "Comment marquer des points :");

    pos++;

    mvwprintw(D->rules, pos++, 6, "- Plus grand village (Cases Ville adjacentes)   : 1*(Nombre de cases du village)");

    pos++;

    if (IS_ALT_V(D->partie)) { /* Si les règles sont alternées */
	mvwprintw(D->rules, pos++, 6, "- Carrés de 4 villes dans la plus grande ville   : 4*(nombre de carrés)");
	pos++;
    }

    if (IS_ALT_F(D->partie)) { /* Si les règles sont alternées */
	mvwprintw(D->rules, pos++, 6, "- Forêts   : La première forêt vaut 1, la seconde 2, la troisième 3, et ainsi de suite.");
	mvwprintw(D->rules, pos++, 6, "             La valeur d'une forêt ne dépasse pas 5.");
    } else {
	mvwprintw(D->rules, pos++, 6, "- Forêts                                        : 2*(Nombre de cases forêt)");
    }

    pos++;

    if (IS_ALT_L(D->partie)) { /* Si les règles sont alternées */
	mvwprintw(D->rules, pos++, 6, "- Lacs                                          : 2*(Nombres de cases Lacs -1)");
    } else {
	mvwprintw(D->rules, pos++, 6, "- Lacs                                          : 3*(Nombres de cases Lacs -1)");
    }

    pos++;

    if (IS_ALT_U(D->partie)) { /* Si les règles sont alternées */
	mvwprintw(D->rules, pos++, 6, "- Ressources / Usines : 4 points par ressource associée à une usine,");
	mvwprintw(D->rules, pos++, 6, "                        sachant qu'une usine accepte deux ressources.");
    } else {
	mvwprintw(D->rules, pos++, 6, "- Ressources / Usines                           : 4*(Nombre de paires R/U)");
    }

    pos++;

    if (IS_ALT_P(D->partie)) { /* Si les règles sont alternées */
	mvwprintw(D->rules, pos++, 6, "- Plaines : 4 points par carré de 4 plaines.");
    }


    pos++;

    mvwprintw(D->rules, pos++, 3, "Contrôles :");

    pos++;

    mvwprintw(D->rules, pos++, 6, "h : Afficher cette aide;             q : Quitter le jeu;");
    mvwprintw(D->rules, pos++, 6, "p : Placer la tuile;                 Flèches directionnelles : Déplacer la tuile;");
    mvwprintw(D->rules, pos++, 6, "r : Tourner la tuile;                n : Changer de tuile;");
    mvwprintw(D->rules, pos++, 6, "x : Enlever la dernière tuile;       ? : Placer automatiquement la tuile suivante");

    wrefresh(D->rules);
    update_panels();
    doupdate();
    getch();
    bottom_panel(D->panels[3]);
    update_panels();
}

