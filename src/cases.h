#ifndef CHONSHU_CASES_H
#define CHONSHU_CASES_H

#include <stdio.h>
#include "structures.h"

/*____
 / ___|__ _ ___  ___
| |   / _` / __|/ _ \
| |__| (_| \__ \  __/
 \____\__,_|___/\___|*/

/*! \brief Une case de la grille.
 *
 * Le grille est une grille de `n`x`n` cases de ce type. Elles ne contiennent les infos que de la dernière Tuile les ayant recouvertes: cela devra peut-être changer. */

typedef struct {

    /*! \brief Son abscisse sur la grille */
    int x;

    /*! \brief Son ordonnée sur la grille */
    int y;

    /*! \brief Une pile contenant l'historique des tuiles recouvrant la case
     *
     * Elle contient des couples (idMove, terrain), qui symbolisent les différents états par laquelle est passée la case. */
    Stack couches;

} Case;

/*____      _ _ _      
 / ___|_ __(_) | | ___ 
| |  _| '__| | | |/ _ \
| |_| | |  | | | |  __/
 \____|_|  |_|_|_|\___| */

/*! \brief La grille de cases dont est formée le plateau
 *
 * On fait une structure `Grille` pour éviter de devoir passer la taille en argument supplémentaire à toutes les fonctions modifiant cette grille */

typedef struct {
    /*! \brief Un entier correspondant à la taille du plateau */
    int taille;

    /*! \brief Une matrice contenant les cases qui constituent le plateau
    */
    Case*** carte;

    Structures villages;

    Structures lacs;
} Grille;

/*____                _                   _
 / ___|___  _ __  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __ ___
| |   / _ \| '_ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__/ __|
| |__| (_) | | | \__ \ |_| |  | |_| | (__| ||  __/ |_| | |  \__ \
 \____\___/|_| |_|___/\__|_|   \__,_|\___|\__\___|\__,_|_|  |___/ */

/*! \brief Initialise une case vide
 *  \param x : son abcisse
 *  \param y : son ordonnée
 *  \return Case * : Une référence vers la case vide
 *
 *  Initialise tous les composants de la case. */

Case* caseInit(int x, int y);

/*! \brief Initialise une grille
 *  \param taille : la taille de la grille
 *  \return Grille * : La grille vide, remplie de cases;
 *
 * La grille contiendra `n`x`n` cases vides. */

Grille* grilleInit(int taille);

/*___            _                   _
|  _ \  ___  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __ ___
| | | |/ _ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__/ __|
| |_| |  __/\__ \ |_| |  | |_| | (__| ||  __/ |_| | |  \__ \
|____/ \___||___/\__|_|   \__,_|\___|\__\___|\__,_|_|  |___/ */

/*! \brief Détruit une case
 * \param C : La case à détruire */

void caseErase(Case* C);

/*! \brief Détruit une grille
 * \param T : la grille à détruire
 *
 * Appelle récursivement `caseErase` pour libérer tout l'espace mémoire. */

void grilleErase(Grille* T);

/*__                  _   _
 / _| ___  _ __   ___| |_(_) ___  _ __  ___ 
| |_ / _ \| '_ \ / __| __| |/ _ \| '_ \/ __|
|  _| (_) | | | | (__| |_| | (_) | | | \__ \
|_|  \___/|_| |_|\___|\__|_|\___/|_| |_|___/ */

/*! \brief Fonction de lecture d'une case sur le Grille
 *
 * \param T Pointeur vers le Grille qui sera lu
 * \param x abcisse de la case à lire
 * \param y ordonnée de la case à lire
 *
 * \return le type de la case (Plaine P, Usine U...) */

char readCase(Grille* T, int x, int y);

/*! \brief Retourne le type de terrain contenu sur la case */

char getType (Case* C);

/*! \brief Retourne le dernier move contenu sur la case */

int getMove (Case* C);

/*! \brief Ajouter une couche aux données d'une case
 *
 * \param C La case à modifier
 * \param moveNb Le numéro de la move recouvrant la case
 * \param terrain Le nouveau terrain de la case
 *
 * Ajoute à la pile interne de la case les informations passées en paramètres. */

void caseUpdate(Case* C, int moveNb, char terrain);

/*! \brief Retirer une couche aux données d'une case
 *
 * \param C La case à modifier
 * \param moveNb Un pointeur vers un entier où stocker le moveNb à retirer
 * \param terrain Un pointeur vers un entier où stocker le terrain à retirer
 *
 * \return `1` si un élément a pu être enlevé, `0` sinon.
 *
 * Enlève à la pile interne de la case un élément. */

int caseRemove(Case* C, int* moveNb, int* terrain);

/*! \brief calculer le nombre de cases de type `type`
 *
 * \param grille La grille à considérer
 * \param type Le type de cases à chercher
 *
 * \return Le nombre de cases de type `type` dans `grille` */

int nbCases(Grille* grille, char type);

#endif
