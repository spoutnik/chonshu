#include "cases.h"

Case* caseInit(int x, int y) {
    Case* C = (Case*) malloc(sizeof(Case));
    
    C->x = x;
    C->y = y;

    C->couches = stackInit();

    return C;
}

Grille* grilleInit(int taille) {
    int i, j;

    Grille* grille = (Grille*) malloc(sizeof(Grille));

    grille->taille = taille;
    
    grille->carte = (Case***) malloc(taille * sizeof(Case**));
    for (i = 0 ; i < taille ; i++) {
	grille->carte[i] = (Case**) malloc(taille * sizeof(Case*));
	for (j = 0 ; j < taille ; j++) {
	    grille->carte[i][j] = caseInit(i, j);
	}
    }

    grille->lacs = structuresInit();
    grille->villages = structuresInit();

    return grille;
}

void caseErase(Case* C) {
    stackErase(C->couches);
    free(C);
}

void grilleErase(Grille* grille) {
    int i, j;

    for (i = 0 ; i < grille->taille ; i++) {
	for (j = 0 ; j < grille->taille ; j++) {
	    caseErase(grille->carte[i][j]);
	}
	free(grille->carte[i]);
    }

    structuresErase(grille->lacs);
    structuresErase(grille->villages);
    free(grille->carte);
    free(grille);
}

char getType(Case* C) {
    int move, terrain;

    if (!peek(C->couches, &move, &terrain))
        return 46;
    else
        return (char) terrain;
}

int getMove(Case* C) {
    int move, terrain;

    if (!peek(C->couches, &move, &terrain))
        return -1;
    else
        return move;
}

void caseUpdate (Case* C, int moveNb, char type) {
    push(C->couches, moveNb, type);
}

int caseRemove(Case* C, int* moveNb, int* type) {
    return pop(C->couches, moveNb, type);
}

int nbCases(Grille* grille, char type) {
    int i, j, compteur = 0;

    for (i = 0; i < grille->taille; i++) {
	for (j = 0; j < grille->taille; j++) {
	    if (getType(grille->carte[i][j]) == type)
		compteur++;
	}
    }

    return compteur;
}

char readCase(Grille* T,int x,int y){
        return getType(T->carte[x][y]);
}
