#ifndef CHONSHU_UI_H
#define CHONSHU_UI_H

#include <ncurses.h>
#include <panel.h>
#include <string.h>
#include "solveur.h"
#include "import.h"

/* __   ___ _   _ _ __ ___  ___  ___ 
| '_ \ / __| | | | '__/ __|/ _ \/ __|
| | | | (__| |_| | |  \__ \  __/\__ \
|_| |_|\___|\__,_|_|  |___/\___||___/ */

/*! \brief Initialise la session `ncurses` 
 *
 * Change les réglages du terminal pour que l'affichage se passe au mieux */

void init();

/*! \brief Finir la session `ncurses`
 *
 * \return Code de sortie
 *
 * Si `done` n'est pas appelée, le terminal agira de façon étrange. Pour remédier à cela, appelez la commande `reset`*/

int done();

/*__                 _                 
 / _| ___ _ __   ___| |_ _ __ ___  ___ 
| |_ / _ \ '_ \ / _ \ __| '__/ _ \/ __|
|  _|  __/ | | |  __/ |_| | |  __/\__ \
|_|  \___|_| |_|\___|\__|_|  \___||___/ */

/*! \brief Dessiner une tuile
 *
 * \param win 		La fenêtre où dessiner la tuile
 * \param P 		La partie où chercher les tuiles
 * \param idTuile 	Le numéro de la tuile à dessiner
 * \param x,y 		Les coordonnées où placer la __case de référence__ de la tuile
 * \param rotation 	La rotation de la tuile à dessiner */

void putTuile(WINDOW* win, Partie* P, int idTuile, int x, int y, int rotation);

/*! \brief Créer une fenêtre
 *
 * \param height,width 	La hauteur et largeur de la fenêtre à créer
 * \param x,y 		Les coordonnées où placer la fenêtre
 *
 * \return Un pointeur vers la fenêtre générée */

WINDOW* createWindow(int height, int width, int y, int x);

/*! \brief Efface le contenu d'une fenêtre
 *
 * \param win La fenêtre à effacer */

void eraseWin(WINDOW* win);

/*! \brief Donner un titre à la fenetre 
 *
 * \param win La fenetre à laquelle donner un titre
 * \param title La chaîne de caractères à afficher */

void setTitle(WINDOW* win, char* title);

/*___  _           _             
|  _ \(_)___ _ __ | | __ _ _   _ 
| | | | / __| '_ \| |/ _` | | | |
| |_| | \__ \ |_) | | (_| | |_| |
|____/|_|___/ .__/|_|\__,_|\__, |
            |_|            |___/ */

/*! \brief Une structure contenant toutes les infos relatives à l'affichage.
 *
 * Le typedef `Display` est un pointeur vers une structure de ce type, pour le passer aux fonctions sans avoir à copier toute la structure. */

typedef struct ui {
    Partie* partie;

    WINDOW* board;
    WINDOW* select;
    WINDOW* status;
    WINDOW* rules;
    WINDOW* scorePane;
    WINDOW* menuBar;

    PANEL* panels[6];

    int cursory, cursorx;
    int tRot;
    int idTuile;
    int gridY, gridX, gridSize;
    int validMove;
}* Display;

/*! \brief Créer un `Display` */

Display initDisplay(Partie* P);

/*! \brief Rafraîchir l'entièreté d'un display
 *
 * \param D Le `Display` à rafraîchir */

void dRefresh(Display D);

/*! \brief Dessiner le plateau de jeu
 *
 * \param D Le `Display` où dessiner la carte.
 * 
 * La map sera dessinée dans la fenêtre `board` du `Display` passé en argument. */

void putMap(Display D);

/*! \brief Initialise le curseur
 *
 * \param D Le `Display` où inittialiser le curseur */

void initCursor(Display D);

/*! \brief Dessiner le curseur
 *
 * \param D Le `Display` où dessiner le curseur */

void putCursor(Display D);

/*! \brief Interpréter les keypresses de l'utilisateur
 *
 * \param D Le `display` à modifier
 * \param c La keypress (obtenue par getch()) */

void interpret(Display D, int c);

/*! \brief Afficher l'historique des `Moves`de la partie
 *
 * \param D Le `Display` où affficher l'historique des placements.
 *
 * L'historique des placements sera affiché dans la fenêtre `status` de D. */

void logMoves(Display D);

/*! \brief Afficher le reste des tuiles disponibles
 *
 * \param D Le `Display` où affficher le reste des tuiles disponibles
 *
 * Le reste des tuiles disponibles sera affiché dans la fenêtre `status` de D. */

void displayRest(Display D);

/*! \brief Afficher la fenêtre des règles du jeu
 *
 * \param D Le `Display` où mettre les règles en avant */

void showRules(Display D);

/*! \brief Affiche une chaine dans une fenetre (coordoonnés d'affichage 2,2 dans la fenetre cible)
 *
 * \param win La fenetre cible
 * \param msg La chaine à afficher dans la fenetre win */
void logWin(WINDOW* win, char* msg);

#endif
