#include "structures.h"

#define max(A,B) ((A) > (B) ? (A) : (B))

Structures structuresInit() {
    Structures S = (Structures) malloc(sizeof(struct linkStructures*));
    *S = NULL;
    return S;
}

void structuresEmpty(Structures S) {
    Stack holder;
    while (structuresDel(S, &holder, 0))
	stackErase(holder);
}

void structuresErase(Structures S) {
    structuresEmpty(S);
    free(S);
}

int structuresAdd(Structures S, Stack casesList) {
    struct linkStructures* new_el = malloc(sizeof(struct linkStructures));
    new_el->casesList = casesList;
    new_el->next = *S;
    *S = new_el;
    return 1;
}
    
int structuresDel(Structures S, Stack* holder, int index) {
    if (index > structuresLen(S) - 1)
	return 0;

    struct linkStructures* tmp = *S;
    struct linkStructures* query;
    while (index > 1 && tmp->next != NULL) {
	tmp = tmp->next;
	index--;
    }

    if (index) {
	query = tmp->next;
	*holder = query->casesList;
	tmp->next = tmp->next->next;
	free(query);
    } else {
	*holder = (*S)->casesList;
	*S = (*S)->next;
	free(tmp);
    }

    return 1;
}

int structuresCheck(Structures S, int x, int y) {
    int index = 0;
    struct linkStructures* current = *S;

    while (current) {

	if (isIn(current->casesList, x, y))
	    return index;

	current = current->next;
	index++;
    }

    return -1;
}

int structuresLen(Structures S) {
    struct linkStructures* current = *S;
    int size = 0;

    while (current && ++size)
	current = current->next;

    return size;
}

int getStructure(Structures S, Stack* holder, int index) {
    struct linkStructures* current = *S;

    while (current && index > 0) {
	current = current->next;
	index--;
    }

    if (index > 0)
	return 0;

    *holder = current->casesList;
    return 1;
}

int tailleMax(Structures listeStructures, int* index) {
    int maxLen = 0, currentLen, depth = 0;

    struct linkStructures* tmp = *listeStructures;

    while (tmp) {
	if ((currentLen = stackLen(tmp->casesList)) > maxLen) {
	    maxLen = currentLen;
	    if (index)
		*index = depth;
	}
        tmp = tmp->next;
	depth++;
    }

    return maxLen;
}
