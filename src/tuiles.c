#include "tuiles.h"

/*! \brief Check for memory integrity
 * \param ptr : Le pointeur à tester
 * \param program_name : Le programme utilisant la mémoire
 *
 * À utiliser sur un pointeur renvoyé par `malloc`. Si le pointeur n'est pas valide, le programme va se terminer en affichant un message d'erreur.
 */

void memcheck(void* ptr, char* program_name) {
    if (!ptr) {
	fprintf(stderr, "%s: Could not allocate memory\n", program_name);
	exit(1);
    }
}

Tuile* tuileInit(int id, char* terrains) {
    int i;

    Tuile* T = (Tuile*) malloc(sizeof(Tuile));

    memcheck((void*) T, "tuileInit");

    T->id = id;

    T->terrains = (char*) malloc(6*sizeof(char));
    memcheck((void*) T->terrains, "tuileInit");

    for (i = 0; i < 6; i++) {
	T->terrains[i] = terrains[i];
    }

    return T;
}

Tuile* tuileInitRandom(int id) {
    int i;
    char terrains[6] = {'P', 'F', 'L', 'V', 'R', 'U'};
    srand(time(NULL));

    Tuile* T = (Tuile*) malloc(sizeof(Tuile));

    memcheck((void*) T, "tuileInit");

    T->id = id;

    T->terrains = (char*) malloc(6*sizeof(char));
    memcheck((void*) T->terrains, "tuileInit");

    for (i = 0; i < 6; i++) {
	T->terrains[i] = terrains[rand()%6];
    }

    return T;
}

void tuileErase(Tuile* T) {
    free(T->terrains);
    free(T);
}
