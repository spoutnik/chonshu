#ifndef CHONSHU_IMPORT_H
#define CHONSHU_IMPORT_H

#include <string.h>
#include <errno.h>
#include "partie.h"

/*! \brief Lire un fichier
 *  \param message : La question à poser à l'utilisateur
 *  \return FILE* le fichier lu */

FILE* readFile(char* message);

/*! \brief Ouvrir un fichier
 *  \param message : La question à poser à l'utilisateur
 *  \return FILE* le fichier lu */

FILE* writeFile(char* message);

/*! \brief Lire un fichier de tuiles
 *  \param raw : Le `file descriptor` du fichier à lire
 *  \return Tuile ** Le jeu de tuiles */

Tuile** tuileImport(FILE* raw);

/*! \brief Lire un fichier de tuiles
 *  \param raw : Le `file descriptor` du fichier à lire
 *  \param deck : un pointeur vers la destination du tas de carte à créer
 *  \return int Le nombre de tuiles lues */

int tuileImportValues(FILE* raw, Tuile*** deck);

/*! \brief Fonction de génération d'un fichier de partie aleatoire
 *
 * \param raw Le fd du fichier à écrire
 *
 * \return 0 si le fichier est généré avec succès
 * \return 1 si une erreur est survenue */

void genPartieFile(FILE* raw);

/*! \brief Fonction de chargement d'une partie à partir d'un fichier
 *
 * \param raw FileDescriptor du fichier de partie
 * \param tuileFile FileDescriptor du fichier de tuiles
 *
 * \return Partie * : une structure Partie avec les differentes donnees (taille grille, taille_main, id carte de départ)
 *
 * \warning On considère le fichier raw comme valide !
 *
 * La partie retournée contient la tuile initiale placée au centre. */

Partie* loadPartieFromFile (FILE* raw, FILE* tuileFile);

/*! \brief Fonction de chargement d'une partie aleatoire
 *
 * \param tuileFile FileDescriptor du fichier de tuile pour correspondance id lu et tuile
 *
 * \return une structure Partie avec les differentes donnees (taille grille, taille_main, id carte de départ)
 *
 * \warning La taille de la grille variera entre 10 et 100
 * \warning La taille de la main variera entre 5 et 15 */

Partie* loadPartieFromRand(FILE* tuileFile);

/*! \brief Écrit dans `target` les Moves de `orig`
 *
 * \param target Le fd du fichier où écrire
 * \param orig La partie à utiliser */

void dumpMoves(FILE* target, Partie* orig);

/*! \brief Écrit dans `target` le terrain de `orig`
 *
 * \param target Le fd du fichier où écrire
 * \param orig La partie à utiliser */

void dumpTerrain(FILE* target, Partie* orig);

/*! \brief Construit `target` suivant les instructions données
 *
 * \param instructions Les instructions à utiliser
 * \param target La partie à construire 
 *
 * `instructions` est un fichier comme celui créé par le solveur, contenant l'historique de placement des tuiles au cours d'une partie.	`rebuild` va alors placer les tuiles aux bons emplacements pour retrouver un état associé aux instructions. */

void rebuild(FILE* instructions, Partie* target);

#endif
