#ifndef CHONSHU_TUILES_H
#define CHONSHU_TUILES_H

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

/*! \brief Un `enum` contenant les types de terrains
 * \enum terrains contient les différentes cases possibles sur les tuiles.
 *
 * Les valeurs contiennent les codes ASCII de leur première lettre (Plaine -> P -> 80). Vide contient '.'. */

enum terrains {PLAINE = 80, FORET = 70, LAC = 76, VILLE = 86, RESSOURCE = 82, USINE = 85, VIDE = 46};

/*! \enum rotations contient les différentes rotations possibles des tuiles.
 *
 * Les valeurs se suivent directement. */

enum rotations {NORMALE=0, DROITE=1, INVERSE=2, GAUCHE=3};

/*____      _ _      
|_   _|   _(_) | ___ 
  | || | | | | |/ _ \
  | || |_| | | |  __/
  |_| \__,_|_|_|\___|*/

/*! \brief Une tuile.
 *
 * On la note:  
 * `0` `1`  
 * `2` `3`  
 * `4` `5`  
 */

typedef struct {
    /* Son numéro */
    int id;

    /*! \brief Un tableau d'entiers qui code les terrains présents.
     *
     * Dans cet ordre:  
     * `0` `1`  
     * `2` `3`  
     * `4` `5`
     */

    char* terrains;

} Tuile;

/*____                _                   _                      
 / ___|___  _ __  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __ ___ 
| |   / _ \| '_ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__/ __|
| |__| (_) | | | \__ \ |_| |  | |_| | (__| ||  __/ |_| | |  \__ \
 \____\___/|_| |_|___/\__|_|   \__,_|\___|\__\___|\__,_|_|  |___/ */

/*! \brief Initialise une tuile
 *  \param id : Le numéro de la tuile
 *  \param cases: Un tableau de 6 `int` contenant les cases de la tuile
 *  \return Tuile * : Une référence vers la tuile
 *
 *  La tuile créée aura une rotation et position par défaut.
 */

Tuile* tuileInit(int id, char* cases);

Tuile* tuileInitRandom(int id);

/*___            _                   _                      
|  _ \  ___  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __ ___ 
| | | |/ _ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__/ __|
| |_| |  __/\__ \ |_| |  | |_| | (__| ||  __/ |_| | |  \__ \
|____/ \___||___/\__|_|   \__,_|\___|\__\___|\__,_|_|  |___/ */

/*! \brief Détruit une tuile
 * \param T : La tuile à détruire
 *
 * Libère aussi la mémoire occupée par le tableau de cases.
 */

void tuileErase(Tuile* T);

#endif
