#include "solveur.h"
#include <string.h>
#include <stdint.h>

#define min(A, B) ((A) < (B) ? (A) : (B))

void genPossibleMoves(Moves possibilites, Partie* P) {
    int i, j, k, rot;

    for (i = 0; i < P->grille->taille; i++) {
	for (j = 0; j < P->grille->taille; j++) {
	    for (rot = 0; rot < 4; rot++) {
		if (valide(P, i, j, rot) == 1) {
		    for (k = 0; k < P->nbTuiles; k++) {
			if (P->tuilesDisponibles[k] > 0)
			    movesAdd(possibilites, k, rot, &P->grille->carte[i][j]);
		    }
		}
	    }
	}
    }
}

int solve(Partie* P) {
    return partialSolve(P, nbTuilesRestantes(P));
}

int partialSolve(Partie* P, int maxDepth) {
    int i, taille = min(nbTuilesRestantes(P), maxDepth+1);
    int depth = 0, max = 0, pts;
    int nbIterations = 0;
    int idTuile, rotation;
    Case** holder;
    FILE* log;
    Moves* possibilites = (Moves*) malloc(taille*sizeof(Moves));

    /* Tableau de Moves
     * À la case i on trouve tous les placements possibles pour placer la ie tuile */
    for (i = 0; i < taille; i++)
	possibilites[i] = movesInit();

    /* On génère les possibilités pour placer la 1e tuile */
    genPossibleMoves(possibilites[0], P);

    /* Tant qu'il reste des possibilités non explorées pour la 1e tuile */
    while (movesLen(possibilites[0])) {

	/* Si on se retrouve à la dernière tuile:
	 * calcul des points de la partie */
	if (depth == maxDepth || partieFin(P)) {
	    nbIterations++;
	    pts = nbPoints(P, 0);

	    if (pts > max) {
		/* On sauvegarde les actions faites */
		log = fopen("optimalMoves.lvl", "w");
		dumpMoves(log, P);
		fprintf(log, "\n%d - %d\n", nbIterations, pts);
		fclose(log);
		max = pts;
	    }

	    enleverTuile(P);
	    depth--;

	} else {
	    switch(movesLen(possibilites[depth])) {
		/* Plus de possibilités, on remonte */
		case 0:
		    enleverTuile(P);
		    depth--;
		    break;

		/* Sinon, on place la tuile et on descend en profondeur */
		default:
		    movesRemove(possibilites[depth], &idTuile, &rotation, &holder);
		    tuileInsertion(P, idTuile, rotation, (*holder)->x, (*holder)->y);
		    if (depth < maxDepth)
			genPossibleMoves(possibilites[++depth], P);
	    }
	}
    }

    log = fopen("optimalMoves.lvl", "a");
    fprintf(log, "\nIterations: %d\n", nbIterations);
    fclose(log);

    for (i = 0; i < taille; i++)
	movesSoftErase(possibilites[i]);

    free(possibilites);

    return max;
}
