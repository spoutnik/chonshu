#ifndef CHONSHU_STACK_H
#define CHONSHU_STACK_H

#include <stdlib.h>

/*! \brief Elément d'une Stack
 * \warning Ne pas utiliser tel quel !
 */

struct link {

    /*! \brief Donnée n°1 du couple
    */
    int x;

    /*! \brief Donnée n°2 du couple 
    */
    int y;

    /*! \brief Un pointeur vers l'élément suivant
     *
     * `NULL` indique la fin de la pile.
     */

    struct link* next;
};

/*! \brief Structure de pile
 *
 * Originellement utilisé pour des coordonnées, un élément contient donc deux entiers (x et y)
 */

typedef struct link** Stack;

/*! \brief Initialise une pile
 */

Stack stackInit();

/*! \brief Copie une pile
 * \param S La pile à copier
 * \return Stack Une nouvelle pile contenant la même chose que S
 * \warning Pas du tout optimisé !
 */

Stack stackCopy(Stack S);

/*! \brief Fusionne deux piles
 * \param dest La pile destination
 * \param toMerge La pile à copier dans `dest`
 * \warning `toMerge` sera détruite ! Seule `dest` restera, avec le contenu des deux piles.
 */

void stackMerge(Stack dest, Stack toMerge);

/*! \brief Détruit une pile
 * \param S La pile à détruire
 */

void stackErase(Stack S);

/*! \brief Ajoute un élément à la pile
 * \param S La pile à modifier
 * \param x L'élément `x` à ajouter
 * \param y L'élément `y` à ajouter
 * \return `1` si l'élément à pu être inséré, `0` sinon.
 */

int push(Stack S, int x, int y);

/*! \brief Enlève un élément d'une pile
 * \param S La pile à modifier
 * \param x Un pointeur vers un entier où stocker `x`
 * \param y Un pointeur vers un entier où stocker `y`
 * \return `1` si l'élément à pu être enlevé, `0` sinon.
 */

int pop(Stack S, int* x, int* y);

/*! \brief Voir le premier élément de la pile
 * \param S La pile dont on veut voir le premier élément
 * \param x Un pointeur vers un entier où stocker `x`
 * \param y Un pointeur vers un entier où stocker `y`
 * \return `1` si l'élément à pu être vu, `0` sinon.
 */

int peek(Stack S, int* x, int* y);

/*! \brief Test de présence dans la pile d'un couple (`x`,`y`)
 * \param S La pile à tester
 * \param x Le premier membre du couple
 * \param y Le second membre du couple
 * \return `1` si l'élément est présent dans la liste, `0` sinon.
 */

int isIn(Stack S, int x, int y);

/*! \brief Taille de la pile
 * \param S La pile
 * \return Sa taille
 */

int stackLen(Stack S);

#endif
