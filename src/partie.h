#ifndef CHONSHU_PARTIE_H
#define CHONSHU_PARTIE_H

#include "placement.h"
#include "tuiles.h"

/*___            _   _      
|  _ \ __ _ _ __| |_(_) ___ 
| |_) / _` | '__| __| |/ _ \
|  __/ (_| | |  | |_| |  __/
|_|   \__,_|_|   \__|_|\___| */

/*! \brief Objet contenant tous les composants nécessaires à une partie */

typedef struct {

    /*! \brief Le terrain, le plateau de jeu */
    Grille* grille;

    /*! \brief Le nombre de tuiles de la partie (= taille de deck) */
    int nbTuiles;

    /*! \brief L'ensemble des tuiles de la partie */
    Tuile** deck;

    /*! \brief Les tuiles disponibles 
     *
     * Un tableau d'entiers, de même taille que `deck`, et qui dit combien fois chaque tuile de la partie est disponible. `tuilesDisponibles[i] = 2` <=> La tuile `deck[i]` est disponible deux fois */
    int* tuilesDisponibles;

    /*! \brief Pile de tous les elements placés lors de la partie (historique des actions) */
    Moves placements;

    char rules;

} Partie;

 /*__            _
|  _ \ ___  __ _| | ___  ___
| |_) / _ \/ _` | |/ _ \/ __|
|  _ <  __/ (_| | |  __/\__ \
|_| \_\___|\__, |_|\___||___/ 
           |___/ */

#define RULES_VILLAGES	1 /* 2^0 */
#define RULES_FORETS	2 /* 2^1 */
#define RULES_LACS	4 /* 2^2 */
#define RULES_USINES	8 /* 2^3 */
#define RULES_PLAINES	16 /* 2^4 */

#define SET_ALT_V(P) ((P)->rules = ((P)->rules | RULES_VILLAGES))
#define SET_ALT_F(P) ((P)->rules = ((P)->rules | RULES_FORETS))
#define SET_ALT_L(P) ((P)->rules = ((P)->rules | RULES_LACS))
#define SET_ALT_U(P) ((P)->rules = ((P)->rules | RULES_USINES))
#define SET_ALT_P(P) ((P)->rules = ((P)->rules | RULES_PLAINES))

#define SET_DEF_V(P) ((P)->rules = (P)->rules & ~RULES_VILLAGES)
#define SET_DEF_F(P) ((P)->rules = (P)->rules & ~RULES_FORETS)
#define SET_DEF_L(P) ((P)->rules = (P)->rules & ~RULES_LACS)
#define SET_DEF_U(P) ((P)->rules = (P)->rules & ~RULES_USINES)
#define SET_DEF_P(P) ((P)->rules = (P)->rules & ~RULES_PLAINES)

#define IS_ALT_V(P) ((P)->rules & RULES_VILLAGES)
#define IS_ALT_F(P) ((P)->rules & RULES_FORETS)
#define IS_ALT_L(P) ((P)->rules & RULES_LACS)
#define IS_ALT_U(P) ((P)->rules & RULES_USINES)
#define IS_ALT_P(P) ((P)->rules & RULES_PLAINES)

/*____                _                   _                      
 / ___|___  _ __  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __
| |   / _ \| '_ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__|
| |__| (_) | | | \__ \ |_| |  | |_| | (__| ||  __/ |_| | | 
 \____\___/|_| |_|___/\__|_|   \__,_|\___|\__\___|\__,_|_|*/

/*! \brief Initialise une partie 
 *
 * \param taille La taille de la grille
 * \param tuileFile Un `file descriptor` vers le fichier de tuiles à lire */

Partie* partieInit(int taille, FILE* tuileFile);

/*___            _                   _                      
|  _ \  ___  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __
| | | |/ _ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__|
| |_| |  __/\__ \ |_| |  | |_| | (__| ||  __/ |_| | |
|____/ \___||___/\__|_|   \__,_|\___|\__\___|\__,_|_| */

/*! \brief Libère l'espace mémoire associé à une partie.
 *
 * \param P La partie à effacer */

void partieErase(Partie* P);

/*___                                                          _   
|  _ \ ___  ___ ___  _   ___   ___ __ ___ _ __ ___   ___ _ __ | |_ 
| |_) / _ \/ __/ _ \| | | \ \ / / '__/ _ \ '_ ` _ \ / _ \ '_ \| __|
|  _ <  __/ (_| (_) | |_| |\ V /| | |  __/ | | | | |  __/ | | | |_ 
|_| \_\___|\___\___/ \__,_| \_/ |_|  \___|_| |_| |_|\___|_| |_|\__| */

/*! \brief Test de validité du placement 
 *
 * \param P La partie où la tuile doit être placée
 * \param rotation La rotation de la tuile à placer
 * \param x,y Les coordonnées de la case de référence de la tuile
 *
 * \return `1` Si le placement est valide
 * \return `-1` Si la tuile sort du terrain
 * \return `-2` Si le tuile n'est pas placée sur une autre tuile
 * \return `-3` Si la tuile est placée sur un lac
 * \return `-4` Si la tuile recouvre entièrement une autre tuile 
 *
 * Cette fonction rassemble tous les tests de placement. */

int valide (Partie* P, int x, int y, int rotation);

/*__                     _   _             
|_ _|_ __  ___  ___ _ __| |_(_) ___  _ __  
 | || '_ \/ __|/ _ \ '__| __| |/ _ \| '_ \
 | || | | \__ \  __/ |  | |_| | (_) | | | |
|___|_| |_|___/\___|_|   \__|_|\___/|_| |_| */

/*! \brief Insertion de tuile
 *
 * \param P Pointeur vers la partie à modifier
 * \param idTuile L'ID de la tuile à placer
 * \param rotation La rotation de la tuile à placer
 * \param x, y La position sur laquelle placer la 'case de référence' de la tuile
 *
 * \return `1` si l'insertion s'est effectuée, `0` sinon
 *
 * \warning Le test de recouvrement n'est pas inclus !
 * 
 * Met à jour la pile des placements et les cases de la grille pour qu'elles contiennent les terrains de la nouvelle tuile. */
  
int tuileInsertion (Partie* P, int idTuile, int rotation, int x, int y);

/*____       _                     
| ____|_ __ | | _____   _____ _ __ 
|  _| | '_ \| |/ _ \ \ / / _ \ '__|
| |___| | | | |  __/\ V /  __/ |   
|_____|_| |_|_|\___| \_/ \___|_| */

/*! \brief Enlever la dernière tuile posée
 *
 * \param P La partie à modifier
 *
 * \return `1` Si la tuile a pu être enlevée, `0` sinon
 *
 * La fonction renverra `0` si seule la première tuile reste. (Pour ne pas rendre la partie injouable) */

int enleverTuile(Partie* P);

/*___                     
/ ___|  ___ ___  _ __ ___ 
\___ \ / __/ _ \| '__/ _ \
 ___) | (_| (_) | | |  __/
|____/ \___\___/|_|  \___| */

/*! \brief Compter les points
 *
 * \param P La partie à considérer
 * \param verbose Si non nul, affiche le détail des points sur stdout
 *
 * \return Le score associé à cette partie */

int nbPoints(Partie* P, int verbose);

/*__                  _   _                 
 / _| ___  _ __   ___| |_(_) ___  _ __  ___ 
| |_ / _ \| '_ \ / __| __| |/ _ \| '_ \/ __|
|  _| (_) | | | | (__| |_| | (_) | | | \__ \
|_|  \___/|_| |_|\___|\__|_|\___/|_| |_|___/ */

/*! \brief Calcule le nombre de tuiles restantes à placer
 *
 * \param P La partie à evaluer
 *
 * \return Le nombre de tuiles restantes à placer */

int nbTuilesRestantes(Partie* P);

/*! \brief Teste si la partie est terminée
 *
 * \param P la partie à tester 
 *
 * \return `1` si la partie est terminée, `0` sinon */

int partieFin(Partie* P);

/*! \brief Numéro de la tuile suivante à placer
 *
 * \param P La `Partie` considérée
 * \param idTuile La tuile précédente
 *
 * \return L'ID de la tuile suivante dans la liste, `-1` sinon
 *
 * Pratique pour automatiser le changement de tuile lors du placement */

int getNextTuile(Partie* P, int idTuile);

/*! \brief Liste les tuiles disponibles dans la partie P
 *
 * \param P La partie à utiliser */

void printDispo(Partie* P);

#endif
