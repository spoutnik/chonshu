#include "import.h"

FILE* fileHandler(char* message, const char* action, const char* messageErreur) {
    char name[1000];
    FILE* fichier;
    
    while (1) {
        printf("%s ", message);
        scanf("%s", name);

        if ( (fichier = fopen(name , action)) != NULL ) {
	    return fichier;
	} else {
	    printf("%s: %s\n", messageErreur, strerror(errno));
	}
    }
    
    return NULL;
}

FILE* readFile(char* message) {
    return fileHandler(message, "r", "Erreur lors de la lecture");
}

FILE* writeFile(char* message) {
    return fileHandler(message, "w", "Erreur lors de l'ouverture");
}

/*! \brief Lire une tuile d'un fichier
 *  \warning Ne marche que si le fichier est initialisé ! Dans le cas général, utilisez plutôt `tuileImport`
 *  \param raw : Le `file descriptor` du fichier à lire
 *  \return Tuile * La tuile lue */

Tuile* tuileRead(FILE* raw) {
    int id, i;
    char cases[6];

    fscanf(raw, "%d\n", &id);

    for (i = 0; i < 6; i += 2) {
	fscanf(raw, "%c %c\n", &cases[i], &cases[i+1]);
    }

    return tuileInit(id, cases);
}

Tuile** tuileImport(FILE* raw) {
    int number;

    fscanf(raw, "%d\n", &number);

    Tuile** deck = (Tuile**) malloc(sizeof(Tuile*));

    while (--number >= 0)
	deck[number] = tuileRead(raw);
    
    return deck;
}

int tuileImportValues(FILE* raw, Tuile*** deck) {
    int i, number;

    fscanf(raw, "%d\n", &number);

    *deck = (Tuile**) malloc(sizeof(Tuile*)*number);

    for (i = 0; i < number; i++)
	(*deck)[i] = tuileRead(raw);
    
    return number;
}

void genPartieFile(FILE* raw){
    int gridSize, handSize, minAvailTuileId, maxAvailTuileId, tuileId, firstTuileId, i;

    minAvailTuileId = 0;
    maxAvailTuileId = 21; 
    
    /* Génération de la taille du terrain et de la main */
    gridSize = (int) (rand() % (100 - 10 + 1)) + 10; 
    handSize = (int) (rand() % (15 - 5 + 1)) + 5;
    fprintf(raw, "%d %d\n", gridSize, handSize);

    /* Génération de la main */
    for (i = 0 ; i < handSize; i ++){
            tuileId = (int) (rand() % (maxAvailTuileId - minAvailTuileId + 1)) + minAvailTuileId;
    	    fprintf(raw, "%d ",tuileId);
    }
    fprintf(raw, "\n");

    /* Génération de la première tuile */
    firstTuileId = (int) (rand() % (maxAvailTuileId - minAvailTuileId + 1)) + minAvailTuileId;
    fprintf(raw, "%d", firstTuileId);

    fclose(raw);
}

Partie* loadPartieFromFile(FILE* raw, FILE* tuileFile){
    Partie* game;
    int gridSize, handSize, tuileId, firstTuileId, i, centerx, centery, rulesCode;

    fscanf(raw, "%d %d\n", &gridSize,&handSize);
    game = partieInit(gridSize,tuileFile);
    for (i = 0; i < handSize - 1; i ++) {
        fscanf(raw, "%d ", &tuileId);
	game->tuilesDisponibles[tuileId] +=1;
    }

    fscanf(raw, "%d\n", &tuileId);
    game->tuilesDisponibles[tuileId] += 1;

    fscanf(raw, "%d\n", &firstTuileId);
    game->tuilesDisponibles[firstTuileId] += 1;

    centerx = (centery = game->grille->taille/2);
    tuileInsertion(game, firstTuileId, NORMALE, centerx, centery-1); /* y - 1 car la case ref de la tuile est en haut à gauche */

    if (fscanf(raw, "%d", &rulesCode) == EOF) {
	game->rules = (char) rand();
	if (game->rules < 0)
	    game->rules = 0 - game->rules;
    } else {
	game->rules = (char) rulesCode;
    }

    return game;
}

Partie* loadPartieFromRand(FILE* tuileFile){
    Partie* game;	
    srand(time(NULL));
    int gridSize, handSize, minAvailTuileId,maxAvailTuileId, tuileId, firstTuileId, i, centerx, centery;
    minAvailTuileId = 0;
    maxAvailTuileId = 20;
    
    gridSize = (int) (rand() % (30 - 10 + 1)) + 10;
    handSize = (int) (rand() % (21 - 10 + 1)) + 10;
    game = partieInit(gridSize,tuileFile);
    for (i = 0 ; i < handSize - 1 ; i ++){
	tuileId = (int) (rand() % (maxAvailTuileId - minAvailTuileId + 1)) + minAvailTuileId;
	game->tuilesDisponibles[tuileId] +=1;
    }
    firstTuileId = (int) (rand() % (maxAvailTuileId - minAvailTuileId + 1)) + minAvailTuileId;
    game->rules = (char) rand();

    if (game->rules < 0)
	game->rules = game->rules*(-1);

    /* Insertion de la tuile */
    centerx = (centery = game->grille->taille/2);
    tuileInsertion(game, firstTuileId, NORMALE, centerx, centery-1); /* y - 1 car la case ref de la tuile est en haut à gauche */
    game->tuilesDisponibles[firstTuileId] += 1;

    return game;
}

void dumpMoves(FILE* raw, Partie* P) {
    int i, idTuile, rotation, size = movesLen(P->placements);
    Case** holder;
    fprintf(raw, "%d\n", size);

    for (i = 0; i < size; i++) {
	movesData(P->placements, i, &idTuile, &rotation, &holder);
	fprintf(raw, "%d %d %d %d\n", idTuile, rotation, holder[0]->x, holder[0]->y);
    }
}

void dumpTerrain(FILE* raw, Partie* P) {
    int i, j;

    for (i = 0; i < P->grille->taille; i++) {
	for (j = 0; j < P->grille->taille; j++) {
	    fprintf(raw, "%c ", readCase(P->grille, j, i));
	}
	fprintf(raw, "\n");
    }
}

void rebuild(FILE* instructions, Partie* target) {
    int i, nb, id, ro, x, y;
    fscanf(instructions, "%d", &nb);
    fscanf(instructions, "%d %d %d %d", &id, &ro, &x, &y);

    for (i = 0; i < nb-1; i++) {
	fscanf(instructions, "%d %d %d %d", &id, &ro, &x, &y);
	tuileInsertion(target, id, ro, x, y);
    }
}
