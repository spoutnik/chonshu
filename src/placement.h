#ifndef CHONSHU_PLACEMENT_H
#define CHONSHU_PLACEMENT_H

#include "cases.h"

/*! \brief Elément d'une pile de placement
 * \warning Ne pas utiliser tel quel !
 */

struct el {

    /*! \brief Le numéro du placement */
    int moveNumber;

    /*! \brief Le numéro de la tuile placée */
    int idTuile;

    /*! \brief L'état de rotation de la tuile placée (NORMALE=0, DROITE=1, INVERSE=2, GAUCHE=3) */
    int rotation;

    /*! \brief La liste des cases où est posée la tuile */
    Case** emplacement;

    /*! \brief Le placement suivant */
    struct el* next;
};

/*! \brief Structure de pile de placement */

typedef struct el** Moves;

/*! \brief Initialise une pile de placement */

Moves movesInit();

/*! \brief Copie une pile de placements
 *
 * \param M La pile à copier
 *
 * \return Moves Une nouvelle pile contenant la même chose que M
 *
 * \warning Pas encore implémenté ! Le sera si nécessaire */

Moves movesCopy(Moves M);

/*! \brief Détruit une pile
 *
 * \param M La pile à détruire */

void movesErase(Moves M);
void movesSoftErase(Moves M);

/*! \brief Ajoute un élément à la pile
 *
 * \param M La pile à modifier
 * \param idTuile Le numéro de la tuile à placer
 * \param rotation La rotation de la tuile à placer
 * \param emplacement La liste des cases où placer la tuile
 *
 * \return La taille de la pile, le numéro de l'élément inséré. */

int movesAdd(Moves M, int idTuile, int rotation, Case** emplacement);

/*! \brief Enlève le premier élément d'une pile
 *
 * \param M La pile à modifier
 * \param idTuile Un pointeur vers un entier où stocker le numéro de la tuile placée
 * \param rotation Un pointeur vers un entier où stocker la rotation de la tuile placée
 * \param emplacement Un pointeur vers un entier où stocker l'emplacement de cette tuile
 *
 * \return `1` si l'élément à pu être enlevé, `0` sinon.
 *
 * \warning Ne pas oublier de libérer le tableau de cases retourné ! */

int movesRemove(Moves M, int* idTuile, int* rotation, Case*** emplacement);

/*! \brief Récupérer les informations sur le placement `i`
 *
 * \param M La pile dont on veut voir les informations
 * \param index Le numéro du placement dont on veut les infos
 * \param idTuile Un pointeur vers un entier où stocker le numéro de la tuile placée
 * \param rotation Un pointeur vers un entier où stocker la rotation de la tuile placée
 * \param emplacement Un pointeur vers un entier où stocker l'emplacement de cette tuile
 *
 * \return `1` si les infos ont pu être récupérées, `0` sinon. */

int movesData(Moves M, int index, int* idTuile, int* rotation, Case*** emplacement);

/*! \brief Renvoie l'`idTuile` associé au placement numéro `index`
 *
 * \param M La pile de Moves où chercher les informations
 * \param index Le numéro du placement dont on cherche les informations
 * 
 * \return L'`idTuile` associé au placement `index` */

int movesIdTuile(Moves M, int index);

/*! \brief Renvoie la `rotation` associé au placement numéro `index`
 *
 * \param M La pile de Moves où chercher les informations
 * \param index Le numéro du placement dont on cherche les informations
 * 
 * \return La `rotation` associé au placement `index` */

int movesRotation(Moves M, int index);

/*! \brief Renvoie l'`emplacement` associé au placement numéro `index`
 *
 * \param M La pile de Moves où chercher les informations
 * \param index Le numéro du placement dont on cherche les informations
 * 
 * \return L'`emplacement` associé au placement `index`
 *
 * \warning Libérer `emplacement` libère le contenu de la Moves ! */

Case** movesEmplacement(Moves M, int index);

/*! \brief Taille de la pile
 *
 * \param M La pile
 *
 * \return La taille de la pile */

int movesLen(Moves M);

#endif
