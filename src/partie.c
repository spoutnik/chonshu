#include "partie.h"
#include "import.h"

#define min(A,B) ((A) < (B) ? (A) : (B))
#define vide(i) (stackLen(emplacement[(i)]->couches) == 0)

/*____                _                   _
 / ___|___  _ __  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __
| |   / _ \| '_ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__|
| |__| (_) | | | \__ \ |_| |  | |_| | (__| ||  __/ |_| | |
 \____\___/|_| |_|___/\__|_|   \__,_|\___|\__\___|\__,_|_|*/

Partie* partieInit(int taille, FILE* tuileFile) {
    Partie* P = (Partie*) malloc(sizeof(Partie));

    P->grille = grilleInit(taille);
    P->nbTuiles = tuileImportValues(tuileFile, &(P->deck));
    P->placements = movesInit();

    P->tuilesDisponibles = (int*) calloc(P->nbTuiles, sizeof(int));

    P->rules = 0;

    return P;
}

/*___            _                   _
|  _ \  ___  ___| |_ _ __ _   _  ___| |_ ___ _   _ _ __
| | | |/ _ \/ __| __| '__| | | |/ __| __/ _ \ | | | '__|
| |_| |  __/\__ \ |_| |  | |_| | (__| ||  __/ |_| | |
|____/ \___||___/\__|_|   \__,_|\___|\__\___|\__,_|_| */

void partieErase(Partie* P) {
    int i;
    grilleErase(P->grille);
    movesErase(P->placements);

    for (i = 0; i < P->nbTuiles; i++)
	tuileErase(P->deck[i]);

    free(P->deck);
    free(P->tuilesDisponibles);
    free(P);
}

/*___                                                          _   
|  _ \ ___  ___ ___  _   ___   ___ __ ___ _ __ ___   ___ _ __ | |_ 
| |_) / _ \/ __/ _ \| | | \ \ / / '__/ _ \ '_ ` _ \ / _ \ '_ \| __|
|  _ <  __/ (_| (_) | |_| |\ V /| | |  __/ | | | | |  __/ | | | |_ 
|_| \_\___|\___\___/ \__,_| \_/ |_|  \___|_| |_| |_|\___|_| |_|\__| */

/*! \brief Voir si une case est recouverte
 *
 *  \param M Pile des placements de la partie
 *  \param idMove Index du placement à considérer
 *  \param caseNb Index de la case à considérer
 *
 *  \return `1` si la case à été recouverte depuis qu'elle a été recouverte par `idMoves`, `0` sinon. */

int recouvert(Moves M, int idMove, int caseNb) {
    Case** emplacement;

    /* On regarde à la case i de M */
    /* On regarde la dernière Move ayant recouvert la case */

    emplacement = movesEmplacement(M, idMove);

    if (idMove == getMove(emplacement[caseNb]))	/* Si la dernière Move est idMove */
	return 0;				/* Alors la tuile n'a pas été recouverte */

    return 1;					/* Sinon la tuile a été recouverte */
}

/*! \brief Test de présence dans un tableau de cases
 *
 * \param c Case à tester
 * \param cases Tableau de pointeurs vers des cases
 *
 * \return `1` si `c` est dans `cases`, `0` sinon */

int notIn(Case* c, Case** cases) {
    int i;

    for (i = 0; i < 6; i++)
	if (c->x == cases[i]->x && c->y == cases[i]->y) return 0;

    return 1;
}

/*! \brief Test de recouvrement
 *
 * \param M Liste des placements effectués
 * \param idMove Le numéro de la move sur laquelle faire le test de recouvrement
 * \param cases Tableau de cases qui doivent être recouvertes
 *
 * \return `0` si la tuile ne sera pas recouverte, `1` sinon
 *
 * \warning Ce test ne teste que si la tuile T va être recouverte, aucune autre !
 * \warning `cases` doit être de taille `6`
 *
 * Teste si la tuile `T` va être recouverte si on place une tuile sur les cases de `cases`.
 * En testant sur toutes les tuiles recouvrant les cases de `cases`, 
 * on peut voir si placer une tuile va recouvrir une autre tuile. */

int tuileRecouvre(Moves M, int idMove, Case** cases) {
    int i;
    Case** emplacement;

    emplacement = movesEmplacement(M, idMove);

    /* Pour chaque case de l'emplacement */
    for (i = 0; i < 6; i++) {
	/* Si la case n'a pas été recouverte est n'est pas dans les cases à recouvrir,
	 * Alors la tuile ne sera pas recouverte  */
	if (!recouvert(M, idMove, i) && notIn(emplacement[i], cases)) return 0;
    }

    /* Toutes les cases sont recouvertes et vont être recouvertes */
    return 1;
}

/*! \brief Met dans `holder` la case de `G` aux coordonnées `x` et `y`
 *
 * \param G Un pointeur vers la grille où chercher la case
 * \param x,y Les coordonnées de la case
 * \param holder Un pointeur vers un emplacement ou stocker la case
 *
 * \return `1` si la case a pu être trouvée, `0` sinon
 *
 * Cette fonction est une façon d'accéder aux cases d'une grille sans risque de segfault. */

int returnCase(Grille* G, int x, int y, Case** holder) {
    if (x >= 0 && y >= 0 && x < G->taille && y < G->taille) {
	*holder = G->carte[x][y];
	return 1;
    } else {
	return 0;
    }
}

/*! \brief Calculer l'emplacement d'une tuile
 *
 * \param G Le terrain où placer la tuile
 * \param rotation La rotation de la tuile
 * \param x,y Les coordonnées de la case de référence de la tuile
 * \param holder Un tableau de `Case*` de taille `6` où stocker les cases destination
 *
 * \return `1` si les cases ont été calculées, `0` sinon
 *
 * Cette fontion fait office de test de limites: si elle renvoit `0`, la tuile dépasse du terrain. */

int getCases(Grille* G, int rotation, int x, int y, Case** holder) {
    int status;
    switch (rotation) {
	case NORMALE:
	    status = returnCase(G, x, y, &holder[0]);
	    status += returnCase(G, x+1, y, &holder[1]);
	    status += returnCase(G, x, y+1, &holder[2]);
	    status += returnCase(G, x+1, y+1, &holder[3]);
	    status += returnCase(G, x, y+2, &holder[4]);
	    status += returnCase(G, x+1, y+2, &holder[5]);
	    return (status == 6);

	case GAUCHE:
	    status = returnCase(G, x, y, &holder[0]);
	    status += returnCase(G, x, y-1, &holder[1]);
	    status += returnCase(G, x+1, y, &holder[2]);
	    status += returnCase(G, x+1, y-1, &holder[3]);
	    status += returnCase(G, x+2, y, &holder[4]);
	    status += returnCase(G, x+2, y-1, &holder[5]);
	    return (status == 6);

	case DROITE:
	    status = returnCase(G, x, y, &holder[0]);
	    status += returnCase(G, x, y+1, &holder[1]);
	    status += returnCase(G, x-1, y, &holder[2]);
	    status += returnCase(G, x-1, y+1, &holder[3]);
	    status += returnCase(G, x-2, y, &holder[4]);
	    status += returnCase(G, x-2, y+1, &holder[5]);
	    return (status == 6);

	case INVERSE:
	    status = returnCase(G, x, y, &holder[0]);
	    status += returnCase(G, x-1, y, &holder[1]);
	    status += returnCase(G, x, y-1, &holder[2]);
	    status += returnCase(G, x-1, y-1, &holder[3]);
	    status += returnCase(G, x, y-2, &holder[4]);
	    status += returnCase(G, x-1, y-2, &holder[5]);
	    return (status == 6);
    }
    return 0;
}

int valide(Partie* P, int x, int y, int rotation) {
    int i, idMove, terrain;
    Case* emplacement[6];

    if (!getCases(P->grille, rotation, x, y, emplacement))
	return -1;

    if (vide(0) && vide(1) && vide(2) && vide(3) && vide(4) && vide(5))
	return -2;

    for (i = 0; i < 6; i++) {
	/* Si une des cases est un lac */
	if (getType(emplacement[i]) == LAC) {
	    return -3;
	}
    }

    for (i = 0; i < 6; i++) {
	/* Pour chaque tuile recouverte (accessible via idMove) */
	if (peek(emplacement[i]->couches, &idMove, &terrain)) {
	    if (tuileRecouvre(P->placements, idMove, emplacement)) {
		return -4;
	    }
	}
    }

    return 1;
}

/*__                     _   _
|_ _|_ __  ___  ___ _ __| |_(_) ___  _ __
 | || '_ \/ __|/ _ \ '__| __| |/ _ \| '_ \
 | || | | \__ \  __/ |  | |_| | (_) | | | |
|___|_| |_|___/\___|_|   \__|_|\___/|_| |_| */

int tuileInsertion (Partie* P, int tuileId, int rotation, int x, int y)
{
    int i, moveNumber;
    Case** emplacement = (Case**) malloc(6*sizeof(Case*));

    if (!getCases(P->grille, rotation, x, y, emplacement)) {
	free(emplacement);
	return 0;
    }

    moveNumber = movesAdd(P->placements, tuileId, rotation, emplacement);
    for (i = 0; i < 6; ++i)
	caseUpdate(emplacement[i], moveNumber, P->deck[tuileId]->terrains[i]);

    P->tuilesDisponibles[tuileId]--;
    return 1;
}

/*____       _
| ____|_ __ | | _____   _____ _ __
|  _| | '_ \| |/ _ \ \ / / _ \ '__|
| |___| | | | |  __/\ V /  __/ |
|_____|_| |_|_|\___| \_/ \___|_| */

int enleverTuile(Partie* P) {
    int i, idTuile, rotation;
    Case** emplacement;

    if (movesLen(P->placements) > 1 && movesRemove(P->placements, &idTuile, &rotation, &emplacement)) {
	P->tuilesDisponibles[idTuile]++;

	for (i = 0; i < 6; i++)
	    pop(emplacement[i]->couches, &idTuile, &rotation);

	free(emplacement);
	return 1;
    }

    return 0;
}

/*___
/ ___|  ___ ___  _ __ ___
\___ \ / __/ _ \| '__/ _ \
 ___) | (_| (_) | | |  __/
|____/ \___\___/|_|  \___| */

int calcPtsLac(Grille* G, int modifieurLac) {
    int somme = 0;

    struct linkStructures* tmp = *(G->lacs);

    while (tmp) {
        somme += modifieurLac*(stackLen(tmp->casesList)-1);
        tmp = tmp->next;
    }

    return somme;
}

int fibo(int n) {
    int i, somme = 0;

    for (i = n; i > 0; i--)
	somme += i;

    return somme;
}

int calcPtsForet(Grille* G) {
    int nbForets = nbCases(G, FORET);

    if (nbForets < 5)
	return fibo(nbForets);
    else
	return 15 + 5*(nbForets - 5);
}


void addPeers(Stack toConsider, Grille* grille, int x, int y, int type) {
    if (x > 0 && getType(grille->carte[x-1][y]) == type)
	push(toConsider, x-1, y);
    if (y > 0 && getType(grille->carte[x][y-1]) == type)
	push(toConsider, x, y-1);
    if (x < grille->taille-1 && getType(grille->carte[x+1][y]) == type)
	push(toConsider, x+1, y);
    if (y < grille->taille-1 && getType(grille->carte[x][y+1]) == type)
	push(toConsider, x, y+1);
}

Stack getStruct(Structures listStruct, Grille* G, int x, int y) {
    Stack toConsider = stackInit();
    push(toConsider, x, y);
    Stack structDetected = stackInit();
    char type = getType(G->carte[x][y]);
    int i, j;

    while (pop(toConsider, &i, &j)) {
	if (!isIn(structDetected, i, j) && structuresCheck(listStruct, i, j) == -1) {
	    push(structDetected, i, j);
	    addPeers(toConsider, G, i, j, type);
	}
    }

    stackErase(toConsider);
    return structDetected;
}

void genStructs(Grille* G, char type) {
    int i, j;
    Structures listStruct = (type == VILLE ? G->villages : G->lacs);

    for (i = 0; i < G->taille; i++) {
	for (j = 0; j < G->taille; j++) {
	    if (getType(G->carte[i][j]) == type && structuresCheck(listStruct, i, j) == -1)
		structuresAdd(listStruct, getStruct(listStruct, G, i, j));
	}
    }
}

void genQuad(Grille* G, Stack S, char type) {
    int i, j;

    for (i = 0; i < G->taille - 1; i++) {
	for (j = 0; j < G->taille - 1; j++) {
	    if (getType(G->carte[i][j]) == type
		    && getType(G->carte[i+1][j]) == type
		    && getType(G->carte[i][j+1]) == type
		    && getType(G->carte[i+1][j+1]) == type) {

		push(S, i, j);
	    }
	}
    }
}

void parseStructs(Grille* G) {
    structuresEmpty(G->villages);
    structuresEmpty(G->lacs);
    genStructs(G, VILLE);
    genStructs(G, LAC);
}

int nbPoints(Partie* P, int verbose) {
    int ptsVille, ptsForet, ptsLac, ptsUsine, ptsPlaine = 0;
    int modifieurLac = 3, modifieurUsine = 1;
    int index, x, y;
    Stack quad = stackInit();

    /* Génerer la liste des structures présentes dans la grille */
    parseStructs(P->grille);

    /* Points des lacs */
    if (IS_ALT_L(P))
	modifieurLac = 2;
    ptsLac = calcPtsLac(P->grille, modifieurLac);

    /* Points des usines */
    if (IS_ALT_U(P))
	modifieurUsine = 2;
    ptsUsine = 4*min(nbCases(P->grille, USINE), modifieurUsine*nbCases(P->grille, RESSOURCE));

    /* Points des forets */
    if (IS_ALT_F(P))
	ptsForet = calcPtsForet(P->grille);
    else
	ptsForet = 2*nbCases(P->grille, FORET);

    /* Points des villes */
    ptsVille = tailleMax(P->grille->villages, &index);
    if (IS_ALT_V(P)) {
	genQuad(P->grille, quad, VILLE);
	while (pop(quad, &x, &y)) {
	    if (structuresCheck(P->grille->villages, x, y) == index)
		ptsVille += 4;
	}
    }

    if (IS_ALT_P(P)) {
	genQuad(P->grille, quad, PLAINE);
	while (pop(quad, &x, &y)) {
	    ptsPlaine += 4;
	}
    }

    if (verbose) {
	printf("Points Ville:\t%d\n", ptsVille);
	printf("Points Forêt:\t%d\n", ptsForet);
	printf("Points Lac:\t%d\n", ptsLac);
	printf("Points Usine:\t%d\n", ptsUsine);
	if (IS_ALT_P(P)) printf("Points Plaine:\t%d\n", ptsPlaine);

	printf("\nScore:\t%d\n", ptsVille+ptsLac+ptsForet+ptsUsine+ptsPlaine);
    }

    stackErase(quad);
    return ptsVille+ptsLac+ptsForet+ptsUsine;
}

/*__                  _   _
 / _| ___  _ __   ___| |_(_) ___  _ __  ___
| |_ / _ \| '_ \ / __| __| |/ _ \| '_ \/ __|
|  _| (_) | | | | (__| |_| | (_) | | | \__ \
|_|  \___/|_| |_|\___|\__|_|\___/|_| |_|___/ */

int nbTuilesRestantes(Partie* P) {
    int somme = 0, i;

    for (i = 0; i < P->nbTuiles; i++)
	somme += P->tuilesDisponibles[i];

    return somme;
}

int getNextTuile(Partie* P, int idTuile) {
    int current = (idTuile + 1)%P->nbTuiles;

    while (current != idTuile) {
	if (P->tuilesDisponibles[current])
	    return current;
	else
	    current = (current + 1)%P->nbTuiles;
    }

    return -1;
}

int partieFin(Partie* P) {
    int i;
    for (i=0; i<P->nbTuiles; i++)
    {
	if (P->tuilesDisponibles[i] != 0) {
	    return 0;
	}
    }
    return 1;
}

void printDispo(Partie* P) {
    int i;

    for (i = 0; i < 12; i++)
	printf("%d ", P->tuilesDisponibles[i]);

    printf("\n");
}
