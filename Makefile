BIN_DIR = bin
SRC_DIR = src
TST_DIR = tests
CC = gcc
CFLAGS = -Wall -Wextra -ansi

$(BIN_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS)

all: chonshu solveur solveurNaif

light:
	rm -rf doc/html doc/latex bin/* rapport.html solveur solveurNaif chonshu optimalMoves.lvl; touch bin/.gitkeep

dox: 
	doxygen doc/Doxyfile

unitTest: $(TST_DIR)/unitTest.c $(BIN_DIR)/cases.o $(BIN_DIR)/stack.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/placement.o $(BIN_DIR)/partie.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/import.o $(BIN_DIR)/structures.o 
	$(CC) $(CFLAGS) -lcunit -lncurses $^ -o $(BIN_DIR)/$@

unitTestCurses: $(TST_DIR)/unitTest.c $(BIN_DIR)/cases.o $(BIN_DIR)/stack.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/placement.o $(BIN_DIR)/partie.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/import.o $(BIN_DIR)/structures.o 
	$(CC) $(CFLAGS) -DCURSES -lcunit -lncurses $^ -o $(BIN_DIR)/unitTest

testFenetre: $(TST_DIR)/testFenetre.c $(BIN_DIR)/ui.o $(BIN_DIR)/stack.o $(BIN_DIR)/cases.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/import.o $(BIN_DIR)/partie.o $(BIN_DIR)/placement.o $(BIN_DIR)/structures.o $(BIN_DIR)/solveur.o
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@ -lncurses -lpanel -DDEBUG

testGenPartieFile: $(TST_DIR)/testGenPartieFile.c $(BIN_DIR)/partie.o $(BIN_DIR)/cases.o $(BIN_DIR)/stack.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/placement.o $(BIN_DIR)/import.o
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@

testSolveur: $(TST_DIR)/testSolveur.c $(BIN_DIR)/partie.o $(BIN_DIR)/cases.o $(BIN_DIR)/stack.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/placement.o $(BIN_DIR)/import.o $(BIN_DIR)/solveur.o $(BIN_DIR)/structures.o
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@

testSDL: $(TST_DIR)/testSDL.c 
	$(CC) $(CFLAGS) $^ -o $(BIN_DIR)/$@ -lSDL2

test: $(SRC_DIR)/executables/chonshu.c $(BIN_DIR)/ui.o $(BIN_DIR)/stack.o $(BIN_DIR)/cases.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/import.o $(BIN_DIR)/partie.o $(BIN_DIR)/placement.o $(BIN_DIR)/structures.o $(BIN_DIR)/solveur.o
	$(CC) $(CFLAGS) $^ -o $@ -lncursesw -lpanel -DDEBUG

chonshu: $(SRC_DIR)/executables/chonshu.c $(BIN_DIR)/ui.o $(BIN_DIR)/stack.o $(BIN_DIR)/cases.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/import.o $(BIN_DIR)/partie.o $(BIN_DIR)/placement.o $(BIN_DIR)/structures.o $(BIN_DIR)/solveur.o
	$(CC) $(CFLAGS) $^ -o $@ -lncursesw -lpanel

solveurNaif: $(SRC_DIR)/executables/solveurNaif.c $(BIN_DIR)/partie.o $(BIN_DIR)/cases.o $(BIN_DIR)/stack.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/placement.o $(BIN_DIR)/import.o $(BIN_DIR)/solveur.o $(BIN_DIR)/structures.o
	$(CC) $(CFLAGS) $^ -o $@ -O3

solveur: $(SRC_DIR)/executables/solveur.c $(BIN_DIR)/partie.o $(BIN_DIR)/cases.o $(BIN_DIR)/stack.o $(BIN_DIR)/tuiles.o $(BIN_DIR)/placement.o $(BIN_DIR)/import.o $(BIN_DIR)/solveur.o $(BIN_DIR)/structures.o
	$(CC) $(CFLAGS) $^ -o $@ -O3
