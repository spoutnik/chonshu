# Rapport PIP - Lot A

__Khelifa__ Mehdi,  
__Chekour__ Yassir,  
__Thibaud__ Paul,  
__Skutnik__ Jean-Baptiste

## Sommaire

- Utilisation
- Présentation
- Répartition du travail
- Description du code
    - Structures finales
    - Choix et organisation
- Tests

## Utilisation

Pour lancer les tests unitaires en version console:

```
make unitTest; <dossier du projet>/bin/unitTest
```

ou avec une interface `ncurses` (si disponible, avec `cunit` compilé pour):

```
make unitTestCurses; <dossier du projet>/bin/unitTest
```

Pour lancer une fenêtre de jeu en `ncurses`:

```
make testFenetre; <dossier du projet>/bin/testFenetre
```

Les commandes sont les suivantes:

- Les flèches déplacent la tuile;
- `h` pour afficher l'aide;
- `x` pour revenir en arrière;
- `p` placer une tuile;
- `n` changer de tuile;
- `r` tourner la tuile;
- `q` quitter le jeu.

## Présentation

Dans le cadre du semestre 2 de la formation ingénieur à l’ENSIIE, le projet informatique consiste en la réalisation d’une application ludique, inspirée du jeu « Honshu », accompagnée de son solveur.

Un plateau de taille `n*n` qui constitue le terrain de jeu. Des tuiles de taille 2x3 cases doivent être placées une à une sur le plateau de jeu de sorte à maximiser le gain en score. Sur chacune des 6 cases de la tuile se trouve un terrain de type Foret (F), Lac (L), Ville (V), Plaine (P), Ressource (R), Usine (U).

Au commencement de la partie, une tuile prédéfinie est située au centre du plateau de jeu. Le joueur dispose de 12 tuiles à placer sur le plateau de jeu une à une, en concordance avec les règles du jeu que voici :
* Toute nouvelle tuile posée doit recouvrir au moins une case de l'une des tuiles posées précédemment,
* Une tuile ne peut être totalement recouverte à aucun moment de la partie,
* Aucune case Lac ne peut être recouverte,
* Les tuiles peuvent faire l'objet de rotation lors de leur placement.

Le but du jeu est de marquer le plus de points possible avec ses tuiles. Voici les règles de comptabilisation des points :
1. On regarde le plus grand village (plus grand bloc de cases Ville contigües). Le joueur marque (nombre de case ville de ce village) points.
2. On compte les Forets. Le joueur marque 2x (nombre de cases forêt) points.
3. On regarde tout les Lacs. Le joueur marque 3x (nombre de cases Lac du lac -1)
points.
4. On compte les Ressources. On alloue une Ressource par Usine tant qu’il reste des cases Ressource et des cases Usine. Chaque Ressource allouée à une Usine rapporte 4points. Une Usine ne peut traiter qu’une Ressource et une Ressource ne peut être allouée qu'à une Usine. Une Usine peut traiter une ressource d’une case qui ne lui est pas contigüe.

## Repartition du travail

La répartition du travail fut aisée grâce à l’outil Trello au-travers duquel nous pouvions clairement voir qui était chargé de procéder à telle ou telle tâche. Nous avons tenté de répartir équitablement le travail, malgré que certaines parties se soient avérées nécessitant beaucoup plus de travail que prévu, ce qui nous a amené à revenir sur la répartition initiale de sorte à pouvoir produire notre rendu dans les délais.

## Planning

Outre les séances destinées au projet informatique (notamment celle du 16/03), nous avons pu mettre en commun à plusieurs reprises les travaux réalisés de sorte à les compléter, les adapter au rendu final, mais également de sorte à faire des mises au point régulières sur l’avancée du projet et des tâches assignées à chacun.

## Description du code

### Structures

Dans le cadre de la mise en place de l'application, nous avons eu recours à l'implémentation de nouvelle structures dont les fonctions sont détaillées ci-dessous :
- `Case`: structure correspondant à une case du plateau de jeu,
- `Grille`: structure correspondant à la grille de cases dont est formée le plateau de jeu,
- `Partie`: structure contenant tous les composants nécessaires à une partie,
- `Tuile`: structure correspondant à une tuile du jeu,
- `Moves`: structure correspondant à la pile de déplacement,
- `Stack`: structure correspond à la pile de couples d'entiers (utilisée pour l'historique local de chaque case faisant intervenir la structure `Link`).
- `struct el`: structure correspondant à un élément de la pile de placement (explicitée dans la suite),
- `struct link`: structure correspondant à un élément de type couple d'entiers d'une pile quelconque.

##   

#### `Case`

```c
typedef struct {
    int x;
    int y;
    Stack couches;
} Case;
```

- Les entiers `x` et `y` correspondent respectivement à l'__abscisse__ et à l'__ordonnée__ de la case en question.
- La pile `couches` contient elle des éléments de type `struct link` correspondant à des couples (identifiant du placement, type de terrain actuel) où le type du terrain actuel correspond au code ASCII de leur première lettre.  

##   

#### `Grille`

```c
typedef struct {
    int taille;
    Case*** carte;
} Grille;
```
- L'entier `taille` correspond à la taille du plateau.
- L'attribut `carte` correspond à une matrice contenant les des pointeurs vers les cases constituant le plateau.

L'intérêt de `Grille` est de ne pas avoir à passer la taille de celle ci en argument de chaque fonction la modifiant: l'objet contient un attribut la représentant.

##   

#### `Partie`

```c
typedef struct {
    Grille* grille;
    int nbTuiles;
    Tuile** deck;
    int* tuilesDisponibles;
    Moves placements;
} Partie;
```

- L'attribut `grille` correspond au plateau de jeu.
- L'entier `nbTuiles` correspond au nombre de tuiles de la partie (soit la taille de `deck` et de `tuilesDisponibles`).
- L'attribut `deck` correspond à un tableau où sont consignées l'ensemble des tuiles chargées lors de la partie en question.
- L'attribut `tuilesDisponibles` est un tableau d'entiers, de même taille que `deck`, et qui indique combien de fois chaque tuile de le partie est disponible.
- L'attribut `placements` correspond à la pile de déplacement pour la partie en cours (l'historique des actions effectuées par le joueur).

##   

#### `Tuile`

```c
typedef struct {
    int id;
    char* terrains;
} Tuile;
```
- L'entier `id` correspond à l'identifiant de la tuile.
- L'attribut `terrains` correspond à un tableau d'entiers qui consigne les terrains présents dans la tuile (on utilise le type `char` pour un gain de mémoire).

`terrains` n'est jamais modifié. Lors du placement, les terrains seront placés selon la rotation désirée:

```
NORMALE | GAUCHE | INVERSE | DROITE
--------|--------|---------|--------
  1 2   |  2 4 6 |   6 5   |  5 3 1
  3 4   |  1 3 5 |   4 3   |  6 4 2
  5 6   |        |   2 1   |
```

##   

#### `Moves`

```c
typedef struct el** Moves;
```

Définition de la __pile de placement__. C'est l'élément central de la partie: elle contient toutes les actions effectuées et permettra de vérifier un placement et revenir en arrière.

Elle est composée d'objets de type `struct el`:

```c
struct el {
    int moveNumber;
    int idTuile;
    int rotation;
    Case** emplacement;
    struct el* next;
};
```
- Les entiers `moveNumber`, `idTuile` et `rotation` correspondent respectivement à l'identifiant de l'action de placement d'une tuile, au numéro de la tuile placée, et à sa rotation.
- `emplacement` correspond à une liste de `6` `Cases * `, cases sur lesquelles est placée la tuile.
- L'élément `next` pointe vers l'élément suivant dans la pile de placement.

##    

#### `Stack`

```c
typedef struct link** Stack;
```

Définition de la structure de pile, constituée d'éléments `struct link` (voir plus bas).

Elle est composée d'éléments de type `struct link`

```c
struct link {
    int x;
    int y;
    struct link* next;
};
```
- Les entiers `x` et `y` correspondent respectivement à la première et à la deuxième donnée du couple stocké dans la pile.
- L'élément `next` pointe vers l'élément suivant dans la pile.

##

### Choix & organisation des Structures

![graphe](http://skutnik.iiens.net/structPartie__coll__graph.png)

Nous avions tout d'abord pensé à stocker les données (coordonnées notamment) de placement de chaque tuile en son sein. Cependant une tuile peut être présente à plusieurs reprises sur le même plateau de jeu: nous avons donc songé à créer une sorte de base de données dans laquelle on conserve l'historique de chaque mouvement. C'est donc en ce sens que nous avons implémenté ce que l'on a nommé __pile de placement__, une pile qui contient l'ensemble des données relatives au placement d'une tuile sur le plateau de jeu.

Lors du placement d'une tuile, une entrée est ajoutée à la pile de déplacement; une fois cette opération effectuée, chaque case de la grille affectée par le placement est mise à jour, en ajoutant à sa pile interne un couple `(moveId, terrain)`, qui correspond à l'index du placement, et au type de terrain que prend la case.

Cette organisation permet d'atteindre toutes les tuiles ayant recouvert une case, et d'atteindre toutes les cases où une tuile est posée, sans conflit, grâce à la pile de placement.

L'objet `Partie` rassemble toutes les informations concernant la partie: les patrons des tuiles (`deck`), leur disponibilité (`tuilesDisponibles`), leur nombre (`nbTuiles`), ainsi que la grille de jeu et la pile de placement.

## Tests

Nous avons également procédé à l'implémentation d'une suite de tests par lot, et ce à l'aide de la librairie `Cunit`. Chaque fonction conçue dans le cadre de l'implémentation du projet est testée, de même que les tests de recouvrement et de limitation de la zone de jeu ont été implémentés.

## Prévisions

Le test de recouvrement est déjà implémenté. Les objectifs pour la suite seront de peaufiner les tests de fuites mémoires, de terminer l'interface `ncurses` et de commencer la conception du solveur.

# Rapport PIP - Lot B

## Sommaire

- Présentation
- Description du code
- Prévisions

## Présentation

Le lot B concerne la réalisation de l'affichage du jeu en terminal, avec les fonctionnalités suivantes:

- Choix du fichier de chargement ou d'un chargement aléatoire au clavier;
- Boucle de jeu :
    - Affichage dans le terminal de la grille et des tuiles restantes à placer;
    - Sélection d'une tuile;
    - Application possible de rotations à la tuile;
    - Entrée des coordonnées d'insertion de la tuile dans la grille;
    - Application des modifications à la grille;
    - Retour en arrière en enlevant les tuiles posées;
    - Test de fin de partie (plus de tuiles à placer).    

Ayant déja commencé à implémenter l'interface graphique grâce à `ncurses`, nous avons directement créé les fonctions d'affichage adaptées à cette interface.   

## Description du code

Le test de recouvrement suit l'algorithme suivant:

```c
emplacement = Tableau de 6 cases a recouvrir par la tuile a placer

For case1 in emplacement
|   tuile = derniere tuile ayant recouvert case1
|
|   For case2 in tuile
|   |	If case2 !recouverte And case2 !(in emplacement)
|   |	|   La tuile ne sera pas recouverte -> case1 suivante
|   |	Endif
|   EndFor
|
|   Aucune case de la tuile ne sera exposee -> return false
EndFor

Aucune tuile ne sera recouverte -> return true
```

Le choix du fichier de chargement se fait grâce à la fonction `readPartieFromFile`, qui prend en arguments le `file descriptor` du fichier de la partie à charger et le `file descriptor` du fichier de tuiles à charger. Le chargement aléatoire se fait avec la fonction `loadPartieFromRand` qui ne prend pour argument que le `file descriptor` du fichier de tuiles, et choisi aléatoirement la partie à utiliser.   

![display](http://skutnik.iiens.net/structui__coll__graph.png)

Une structure `Display` contient toutes les informations relatives à l'affichage, dont la position du curseur où placer la tuile et sa rotation.

L'affichage de la grille se fait avec la fonction `putMap`, qui prend en argument le `Display`, pointeur vers une structure qui contient toutes les infos relatives à l'affichage.

La selection d'une tuile se fait toute seule, une tuile étant renseignée en attribut dans la structure `Display`. Pour changer de tuile selectionnée, on utilise la fonction `getNextTuile`, qui prend en argument la partie considérée et la tuile précédente, pour retourner l'id de la tuile suivante dans la liste des tuiles disponibles de la structure `Partie`. Cela permet, sur l'inteface `ncurses`, d'attribuer une touche à cette fonction pour pouvoir naviguer facilement parmi les tuiles disponibles.

Les rotations de la tuile à placer se font grâce à la fonction `updateRot`, qui prend en argument le `Display`, et change la position de la tuile affichée pour la mettre en position suivante. Elle ne prend pas en argument la position dans laquelle on veut mettre la tuile, il faudra l'utiliser plusieurs fois pour obtenir la position souhaitée. Cela permet, toujours sur ncurses, d'attribuer une touche à la rotation de la tuile et de modifier sa position succéssivement en utilisant cette touche.

L'entrée des coordonnées d'insertion de la tuile dans la grille se fait grâce à la fonction `tuileInsertion`, qui prend en argument la partie, l'id de la tuile à insérer, la rotation voulue, et la coordonnée de la case de référence de la tuile (cf. la structure d'une tuile, la case de référence est la case 1). Cette fonction met à jour la pile des placements et insère sur chaque case concernée les terrains de la nouvelle tuile.

Le test de fin de partie s'effetue dans la fonction `partieFin`, qui prend la partie en argument et vérifie que les éléments de la liste tuilesDisponibles sont bie tous nuls.

## Prévisions

Pour la site, nous avons déjà commencé à mettre en place un système de score, et l'interface ncurses est prête et n'attend que les modifications apportées au code dans les prochains lots.

# Rapport PIP - Lot C

## Sommaire

- Présentation
- Description du code
- Prévisions

## Présentation

Le lot C présente comme objectifs:

- Le comptage des points accumulés
- La réalisation d'un solveur naïf
- La réalisation d'un solveur optimisé

## Description du code

### Score

Lors du calcul du score, le challenge réside dans la lecture des villages et des lacs adjacents présents dans la grille. On utilisera par la suite le terme de 'structure' pour parler de cases adjacente de même type.

Pour modéliser ces structures en mémoire, on crée un type `Structures`, qui est essentiellement une pile de `Stack`s. À chaque structure de la grille on associe une `Stack` dans `Structures`, contenant les coordonnées de chaque case de la structure.

Diagramme UML du type `Structures`:
![Strutures](http://skutnik.iiens.net/structure.png)

On utilise `parseStructs` pour générer les `Structures` associées aux villes et lacs de la grille: il suffit alors de regarder les tailles des `Stacks` crées pour calculer les points gagnés.

### Solveur naïf

Le solveur naïf est implémenté selon l'algorithme donné dans le sujet, en utilisant les objets créés précédemment dans le projet: on utilise `Moves` pour stocker les possibilités de placement. Une fois toutes les possibilités effectuées, on renvoie le maximum des scores atteints.

L'algorithme va de plus créer un fichier `optimalMoves.lvl` où il va stocker les placements effectués pour arriver à ce score. Ce fichier va permettre par la suite de créer la partie correspondante pour l'étudier ou la charger pendant le jeu.

### Solveur optimisé

Le meilleur solveur optimisé que nous ayons testé est celui implémentant l'algorithme précédent pour une seule étape, à chaque étape. Il va effectuer le placement rapportant le plus de points à chaque étape du jeu.

Il utilise donc le code du solveur précédent, qui a été adapté et qui contient un argument `maxdepth`, qui symbolise la profondeur maximale à atteindre dans l'arbre des possibilités, soit ici le nombre de tuiles à placer.

Un solveur se concentrant sur une composante du score (Lacs, Villages, etc) le fait au détriment des autres, et n'atteint jamais un score aussi élevé que celui implémenté ici.

## Prévisions

Pour permettre l'implémentation des variantes de façon efficace, le calcul des points est séparé en calcul de chaque composante du score, qui favorise aussi la clarté et la description de la partie à l'utilisateur.

# Rapport PIP - Lot D

## Sommaire 

- Présentation 
- Description du code 

## Présentation 

Le lot D nous laisse le choix sur l'amélioration à apporter au jeu:

- Implémenter une interface graphique pour que je jeu soit jouable à la souris
- Créer un système de variantes dans les règles de comptage de points du jeu

Étant donné le fait que nous avions déjà implémenté une interface graphique grâce à la bibliothèque ncurses, nous avons choisi de nous pencher sur la modification des règles du jeu pour en créer des variantes. 

Les modifications de règles à prendre en compte sont:

- Une case par Lac vaut 2pts (override règle 3)
- Une plaine de quatre cases vaut 4pts (nouvelle règle)
- Une usine peut accepter deux ressources (override règle 4)
- Un carré de quatre cases village, s'il est dans la ville comptabilisée, donne 4pts bonus (ajout à la règle 1)
- Pour chaque forêt, la première case vaut 1pt, la seconde 2pts, la troisième 3pts, etc. (max5) (override règle 2)

## Présentation du code 

Nous avons voulu faire en sorte que non seulement le jeu puisse se jouer avec l'une des variantes citées ci-dessus ou sans modification, mais aussi pouvoir le jouer en mélangeant certaines variantes. 

Pour ce faire, nous avons choisi de signifier l'application ou non de la nouvelle règle pour chaque type de terrain (village, usine, ressource, plaine, foret) par un bit d'un entier rentré dans le fichier de Partie. 

Par exemple, le fichier Partie3 contient l'entier `43`, qui se traduit par 101011 en binaire. On regarde les 5 derniers bits de cet entier, à savoir `01011`, et on l'interprête: 

- `0`: La nouvelle règle concernant le comptage des points pour les plaines ne sera pas activée pour cette partie; 
- `1`: La règle concernant le comptage des points pour les usines sera remplacée par la nouvelle règle; 
- `0`: La règle concernant le comptage des points pour les lacs restera la même; 
- `1`: La règle concernant le comptage des points pour les forêts sera remplacée par la nouvelle règle; 
- `1`: La règle concernant le comptage des points pour les villages sera remplacée par la nouvelle règle. 

Nous avons implémenté ce système grâce à des fonctions définies en macro : 

### Définition de variables pour relier les différentes règles aux bits correspondants: 

```c 
#define RULES_VILLAGES	1 /* 2^0 */
#define RULES_FORETS	2 /* 2^1 */
#define RULES_LACS	4 /* 2^2 */
#define RULES_USINES	8 /* 2^3 */
#define RULES_PLAINES	16 /* 2^4 */
```
### Fonctions de rajout de règles

Exemple pour les villages:
```c
#define SET_ALT_V(P) ((P)->rules = ((P)->rules | RULES_VILLAGES))
```

### Fonctions de suppression de règles

Exemple pour les villages:
```c
#define SET_DEF_V(P) ((P)->rules = (P)->rules & ~RULES_VILLAGES)
```

### Fonctions de vérification de l'activation de règles

Exemple pour les villages:
```c
#define IS_ALT_V(P) ((P)->rules & RULES_VILLAGES)
```

Une fois ces macros obtenues, il a suffit de modifier le comptage des points en testant l'activation des règles grâce à la fonction ci-dessus, et effectuer des mineures modifications pour compter les points, comme le test de carré de quatre cases village par exemple. 
L'affichage des règles dans l'interface de jeu tient également compte des règles activée pour la partie, toujours grâce aux macros de test. 
