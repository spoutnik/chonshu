map=$(cat $1)

echo "Villages:"
echo "$(echo "$map" | sed -e "s/[^V ]/./g")"
echo
echo "Lacs:"
echo "$(echo "$map" | sed -e "s/[^L ]/./g")"
echo
echo "Forêts:"
echo "$(echo "$map" | sed -e "s/[^F ]/./g")"
echo
echo "Usines:"
echo "$(echo "$map" | sed -e "s/[^UR ]/./g")"
echo
echo "Plaines:"
echo "$(echo "$map" | sed -e "s/[^P ]/./g")"
